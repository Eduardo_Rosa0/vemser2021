import axios from 'axios';

const endereco = 'http://localhost:9000/api';

export default class EpisodioAPI {
  buscar() {
    return axios.get( `${ endereco }/episodios` ).then( e => e.data );
  }

  registrarNota( { nota, episodioId } ) {
    return axios.post( `${ endereco }/notas`, { nota, episodioId } );
  }

  episodioDetalhes( episodioId ) {
    return axios.get(`${endereco}/episodios/${episodioId}/detalhes`).then(e => e.data[0]);
  }

}