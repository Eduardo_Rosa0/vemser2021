import React from 'react';

import BotaoUi from '../../Components/BotaoUi';

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state;

  return (
    <React.Fragment>
      <BotaoUi classe="preto" link="/" nome="Página Inicial" />
      <br/>
      {
        listaEpisodios.avaliados && (
          listaEpisodios.avaliados.map(episodio => {

            return (
              <BotaoUi
                key={episodio.id}
                classe="preto"
                link={{ pathname: "/detalhe", state: { episodio } }}
                nome={`${episodio.nome} - ${episodio.media}`}
              />
            )
          })
        )
      }
    </React.Fragment>
  );
}

export default ListaAvaliacoes;