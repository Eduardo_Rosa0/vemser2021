import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import ListaAvaliacoes from './Avaliacoes';
import TelaDetalheEpisodio from '../Components/TelaDetalheEpisodio';
import TodosEpisodios from './TodosEpisodios';

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" component={ ListaAvaliacoes } />
        <Route path="/detalhe" component={ TelaDetalheEpisodio } />
        <Route path="/todos" component={ TodosEpisodios } />
      </Router>
    );
  }
}

/* const PaginaTeste = () =>
  <div>
    <h1>Página Teste</h1>
    <Link to="/">Página App</Link>
  </div> */