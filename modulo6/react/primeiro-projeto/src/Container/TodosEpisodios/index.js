import React from 'react';

import BotaoUi from '../../Components/BotaoUi';
import EpisodioAPI from '../../models/EpisodioAPI';

const TodosEpisodios = props => {

  const { listaEpisodios } = props.location.state;


  const ordenarASC = params => {
    const { array, propert } = params;
    return array.sort( ( a, b ) => a[`${propert}`] - b[`${propert}`]);
  }

  const ordenarDESC = params => {
    const { array, propert } = params;
    return array.sort( ( a, b ) => a[`${propert}`] - b[`${propert}`]);
  }


  return (
    <React.Fragment>
      <BotaoUi classe="preto" link="/" nome="Página Inicial" />

      <button className='btn preto'  >
        Ordenar por data de estréia
      </button>

      <button className='btn preto'  >
        Ordenar por duração
      </button>

      <hr/>
      { 
        this.state.listaEpisodios.todos.map(episodio => {

          return (
            <BotaoUi
              key={episodio.id}
              classe="preto"
              link={{ pathname: '/detalhe', state: { episodio } }}
              nome={`${episodio.nome}`}
            />
          )
        })
      }
    </React.Fragment>
  );
}

export default TodosEpisodios;