import React, { Component } from 'react';
import EpisodioAPI from '../../models/EpisodioAPI';

export default class TelaDetalheEpisodio extends Component {
  constructor(props) {
    super(props);
    this.episodioAPI = new EpisodioAPI();
    this.state = {
      carregado: false,
      episodio: this.props.location.state['episodio']
    }
  }

  componentDidMount() {
    this.episodioAPI.episodioDetalhes(this.state.episodio.id)
      .then(data => {

        const { episodio } = this.state;

        episodio.notaImdb = data.notaImdb;
        episodio.sinopse = data.sinopse;
        episodio.dataEstreia = data.dataEstreia;

        this.setState(state => {
          return {
            ...state,
            carregado: true,
            episodio: episodio
          }
        })
      })
  }

  render() {
    const { episodio, carregado } = this.state;
    return (
      carregado
        ? (
        <React.Fragment>
          <h2>Nome: {episodio.nome}</h2>
          <img src={episodio.imagem} alt={episodio.nome} />
          <p>Temporada/Episódio: {episodio.temporadaEpisodio}</p>
          <p>Duração: {episodio.duracaoEmMin}</p>
          <p>Sinopse: {episodio.sinopse}</p>
          <p>Data de estreia: {new Intl.DateTimeFormat('pt-BR').format(new Date(episodio.dataEstreia))}</p> 
          <p>Nota: {episodio.media}</p>
          <p>Nota do IMDB: {episodio.notaImdb}</p>
          <p>Nota do IMDB: {((episodio.notaImdb / 10) * 5).toFixed(2)}</p>
          
        </React.Fragment>
        )
        : (<h3>Aguarde...</h3>)
    );
  }
}


