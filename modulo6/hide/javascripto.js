//Exercício 01
const calcularCirculo = params => {
    const { raio, tipoCalculo } = params;

    if (tipoCalculo != 'A' && tipoCalculo != 'C')
        return null;

    return tipoCalculo == 'A' ? (Math.PI * raio * raio) : (2 * Math.PI * raio);
}

//Exercício 02
const naoBissexto = number => 
    ((number % 400 == 0) || ((number % 4 == 0) && (number % 100 != 0)));

//Exercício 03
const somarPares = (array = []) => {
    let count = 0;

    array.forEach((element, index, array) => {
        if (index % 2 == 0)
            count += element;
    });
    return count;
}

//Exercício 04
const somaDiferentona = value1 => value2 => value1 + value2;

//Exercício 05
const imprimirBRL = value => {
    value = Math.ceil( value * 100) / 100;
    const result = value.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });

    console.log(result);
}
