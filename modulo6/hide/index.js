const cardapioIFood = (veggie = true, comLactose = true) =>{
    let cardapio = ["enroladinho de salsicha", "cuca de uva"];    

    //cardapio.push(...["pastel de carne", "empada de legumes marabijosa"]);
    cardapio = [...cardapio, "pastel de carne", "empada de legumes marabijosa" ];

    if (comLactose) 
        cardapio.push("pastel de queijo");
    if (!veggie)
        return cardapio.map(i => i.toUpperCase());

    const indiceEnroladinho = cardapio.indexOf("enroladinho de salsicha");
    cardapio.splice(indiceEnroladinho, 1);

    const indicePastelCarne = cardapio.indexOf("pastel de carne");
    cardapio.splice(indicePastelCarne, 1);

    //cardapio = cardapio.filter(i => i !== 'enroladinho de salsicha' && i !== 'pastel de carne');

    return cardapio.map(i => i.toUpperCase());
}

console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
