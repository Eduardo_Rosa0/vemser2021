
const renderizar = ( pokemon ) => {
  document.querySelector( '#poke-id' ).innerHTML = `#${ pokemon.id }`;

  document.querySelector( '#poke-nome' ).innerHTML = `${ pokemon.nome }`;

  document.querySelector( '#poke-img' ).src = pokemon.imagem;

  document.querySelector( '#poke-img' ).classList.remove( 'none' );

  document.querySelector( '#poke-altura' ).innerHTML = `Altura: ${ pokemon.altura }cm`;

  document.querySelector( '#poke-peso' ).innerHTML = `Peso: ${ pokemon.peso }Kg`;

  pokemon.tipos.forEach( ( element ) => {
    const li = document.createElement( 'li' );
    const p = document.createElement( 'p' );

    p.innerHTML = element.toUpperCase();

    p.classList.add( 'poke-tipo' );

    li.appendChild( p );
    document.querySelector( '#poke-tipo-list' ).appendChild( li );
  } );


  pokemon.estatisticas.forEach( ( element ) => {
    const li = document.createElement( 'li' );
    const p = document.createElement( 'p' );

    p.innerHTML = `${ element.nome }: ${ element.valor }`;

    p.classList.add( 'poke-info' );

    li.appendChild( p );
    document.querySelector( '#poke-info-list' ).appendChild( li );
  } );
};

const clear = () => {
  document.querySelector( '#poke-id' ).innerHTML = '';

  document.querySelector( '#poke-nome' ).innerHTML = '';

  document.querySelector( '#poke-img' ).src = '';

  document.querySelector( '#poke-img' ).classList.add( 'none' );

  document.querySelector( '#poke-altura' ).innerHTML = '';

  document.querySelector( '#poke-peso' ).innerHTML = '';

  document.querySelector( '#poke-tipo-list' ).innerHTML = '';

  document.querySelector( '#poke-info-list' ).innerHTML = '';
};

const findById = () => {
  const id = document.querySelector( '#input-id' );
  // eslint-disable-next-line no-undef
  const fetchPokemon = new FetchPokemon();
  const pokeId = document.querySelector( '#poke-id' );

  if ( id.value === '' ) {
    clear();
    return;
  }

  if ( id.value > 0 ) {
    console.error( `id.value: ${ id.value > 0 }` );
    id.value = '';
    clear();
    return;
  }

  if ( pokeId.innerHTML.replace( '#', '' ) === id.value ) return;

  fetchPokemon.buscarEspecifico( id.value ).then( ( pokemon ) => {
    clear();
    renderizar( new Pokemon( pokemon ) );
  } );
};

document.querySelector( '#input-id' ).addEventListener( 'blur', () => findById() );

const random = () => {
  let min = 1;
  let max = 893;
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
};

document.querySelector( '#random' ).addEventListener( 'click', () => {
  // eslint-disable-next-line no-undef
  const fetchPokemon = new FetchPokemon();
  let pokeNumber = 0;

  let pokeSortudosLocalStorage = localStorage.getItem( 'pokeSortudos' );
  pokeSortudosLocalStorage = pokeSortudosLocalStorage
    ? JSON.parse( pokeSortudosLocalStorage )
    : null;
  let pokeSortudos = pokeSortudosLocalStorage || [];

  if ( pokeSortudos.length >= 893 ) pokeSortudos = [];

  do {
    pokeNumber = random();
    if ( pokeSortudos.includes( pokeNumber ) ) console.log( pokeNumber );
  } while ( pokeSortudos.includes( pokeNumber ) );

  pokeSortudos.push( pokeNumber );

  localStorage.setItem( 'pokeSortudos', JSON.stringify( pokeSortudos ) );

  fetchPokemon.buscarEspecifico( pokeNumber ).then( ( pokemon ) => {
    clear();
    renderizar( new Pokemon( pokemon ) );
  } );
} );
