class Pokemon { // eslint-disable-line no-unused-vars
  constructor( params ) {
    this._id = params.id;
    this._nome = params.name;
    this._imagem = `https://pokeres.bastionbot.org/images/pokemon/${ params.id }.png`;
    this._altura = params.height;
    this._peso = params.weight;
    this._tipos = params.types.map( type => type.type.name );
    this._estatisticas = params.stats.map( statElement => ( {
      nome: statElement.stat.name,
      valor: statElement.base_stat,
    } ) );
  }

  get id() {
    return `${ this._id }`;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${ this._altura * 10 }`;
  }

  get peso() {
    return `${ this._peso }`;
  }

  get tipos() {
    return this._tipos;
  }

  get estatisticas() {
    return this._estatisticas;
  }
}
