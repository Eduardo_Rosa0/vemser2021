class FetchPokemon { // eslint-disable-line no-unused-vars
  constructor() {
    this._url = 'https://pokeapi.co/api/v2/pokemon';
  }

  buscarEspecifico( id ) {
    const requisicao = fetch( `${ this._url }/${ id }` );

    fetch( `${ this._url }/${ id + 1 }` )
      .then( ( data ) => data.json() )
      .then( data => console.log( data ) )

    return requisicao.then( ( data ) => data.json() );
  }
}
