package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/item")
public class ItemController {

    @Autowired
    private ItemService service;

    @GetMapping( value = "/all")
    @ResponseBody
    public List<ItemDTO> findAll(){
        return service.findAll();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public ItemDTO findById( @PathVariable Integer id){
        try {
            return service.findById(id);
        }catch ( ItemNaoEncontrado e){
            System.err.println(e.getMensagem());
            return null;
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ItemDTO save( @RequestBody ItemEntity item ){
        return service.save( item );
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ItemDTO update( @RequestBody ItemEntity item, @PathVariable  Integer id ){
        return service.update( item, id );
    }

    @GetMapping( value = "/descricao/{ descricao }")
    @ResponseBody
    public ItemDTO findByDescricao( @PathVariable String descricao ){
        return service.findByDescricao(descricao);
    }

    @GetMapping( value = "/all/descricao/{ descricao }")
    @ResponseBody
    public List<ItemDTO> findAllByDescricao( @PathVariable String descricao ){
        return service.findAllByDescricao(descricao);
    }

    @PostMapping( value = "/all/descricoes")
    @ResponseBody
    public List<ItemDTO> findAllByDescricaoIn( @RequestBody List<String> descricoes ){
        return service.findAllByDescricaoIn(descricoes);
    }

    @PostMapping( value = "/inventario/item")
    @ResponseBody
    public ItemDTO findByInventarioItemIn( @RequestBody List<Inventario_X_Item> inventarioItem ){
        return service.findByInventarioItemIn(inventarioItem);
    }

    @PostMapping( value = "/all/inventario/item")
    @ResponseBody
    public List<ItemDTO> findAllByInventarioItemIn( @RequestBody List<Inventario_X_Item> inventarioItem ){
        return service.findAllByInventarioItemIn(inventarioItem);
    }
}
