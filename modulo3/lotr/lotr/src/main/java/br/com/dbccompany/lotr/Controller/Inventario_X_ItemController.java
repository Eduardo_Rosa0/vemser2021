package br.com.dbccompany.lotr.Controller;


import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Services.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/inventario/item")
public class Inventario_X_ItemController {
    @Autowired
    private Inventario_X_ItemService service;

    @GetMapping( value = "/all")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAll(){
        return service.findAll();
    }

    @PostMapping( value = "/")
    @ResponseBody
    public Inventario_X_ItemDTO findById(@RequestBody Inventario_X_ItemId id){
        return service.findById(id);
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public Inventario_X_ItemDTO save( @RequestBody Inventario_X_Item inventarioItem ){
        return service.save( inventarioItem );
    }

    @PutMapping( value = "/editar")
    @ResponseBody
    public Inventario_X_ItemDTO update( @RequestBody Inventario_X_Item inventario, @RequestBody  Inventario_X_ItemId id ){
        return service.update( inventario, id );
    }

    //------------
    @PostMapping( value = "/inventario")
    @ResponseBody
    public Inventario_X_ItemDTO findByInventario( @RequestBody InventarioEntity inventario){
        return service.findByInventario(inventario);
    }

    @PostMapping( value = "/all/inventario")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByInventario( @RequestBody InventarioEntity inventario){
        return service.findAllByInventario(inventario);
    }

    @PostMapping( value = "/all/inventarios")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByInventarioIn( @RequestBody List<InventarioEntity> inventarios){
        return service.findAllByInventarioIn(inventarios);
    }

    @PostMapping( value = "/item")
    @ResponseBody
    public Inventario_X_ItemDTO findByItem( @RequestBody ItemEntity item ){
        return service.findByItem(item);
    }

    @PostMapping( value = "/all/item")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByItem( @RequestBody ItemEntity item ){
        return service.findAllByItem(item);
    }

    @PostMapping( value = "/all/item/in")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByItemIn( @RequestBody List<ItemEntity> itens ){
        return service.findAllByItemIn(itens);
    }

    @GetMapping( value = "/quantidade/{ quantidade }")
    @ResponseBody
    public Inventario_X_ItemDTO findByQuantidade( @PathVariable Integer quantidade ){
        return service.findByQuantidade(quantidade);
    }

    @GetMapping( value = "/all/quantidade/{ quantidade }")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByQuantidade( @PathVariable Integer quantidade ){
        return service.findAllByQuantidade(quantidade);
    }

    @PostMapping( value = "/all/quantidade/in")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByQuantidadeIn( @RequestBody List<Integer> quantidades ){
        return service.findAllByQuantidadeIn(quantidades);
    }
}
