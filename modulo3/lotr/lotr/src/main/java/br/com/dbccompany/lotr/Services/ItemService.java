package br.com.dbccompany.lotr.Services;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemService{

    @Autowired
    private ItemRepository repository;
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8081/salvar/logs";

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO save(ItemEntity item ){
        return this.saveUpdate(item);
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO update( ItemEntity item, Integer id ){
        item.setId(id);
        return this.saveUpdate(item);
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO saveUpdate( ItemEntity item ){
        return new ItemDTO(repository.save( item ));
    }

    public ItemDTO findById( Integer id ) throws ItemNaoEncontrado {
        try {
            return new ItemDTO(repository.findById(id).get());
        }catch ( Exception e ){
            throw new ItemNaoEncontrado();
        }
    }

    public List<ItemDTO> findAll(){
        return  parseList(repository.findAll());
    }

    public ItemDTO findByDescricao( String descricao ){
        return new ItemDTO(repository.findByDescricao(descricao));
    }

    public List<ItemDTO> findAllByDescricao( String descricao ){
        return parseList(repository.findAllByDescricao(descricao));
    }

    public List<ItemDTO> findAllByDescricaoIn( List<String> descricoes ){
        return parseList(repository.findAllByDescricaoIn(descricoes));
    }

    public ItemDTO findByInventarioItemIn( List<Inventario_X_Item> inventarioItem ){
        return new ItemDTO(repository.findByInventarioItemIn(inventarioItem));
    }

    public List<ItemDTO> findAllByInventarioItemIn(List<Inventario_X_Item> inventarioItem ){
        return parseList(repository.findAllByInventarioItemIn(inventarioItem));
    }

    public List<ItemDTO> parseList( List<ItemEntity> itens){
        if( itens == null ) return null;

        List<ItemDTO> dtos = new ArrayList<>();
        for(ItemEntity item : itens){
            dtos.add(new ItemDTO(item));
        }
        return dtos;
    }

}
