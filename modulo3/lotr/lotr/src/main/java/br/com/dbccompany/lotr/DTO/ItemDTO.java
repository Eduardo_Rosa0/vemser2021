package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;

import java.util.List;

public class ItemDTO {
    private String descricao;
    private List<Inventario_X_Item> inventarioItem;

    public ItemDTO( ItemEntity item ) {
        this.descricao = item.getDescricao();
    }

    public ItemEntity converter() {
        ItemEntity item = new ItemEntity();
        item.setInventarioItem(this.inventarioItem);
        item.setDescricao(this.descricao);
        return item;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Inventario_X_Item> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_Item> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}
