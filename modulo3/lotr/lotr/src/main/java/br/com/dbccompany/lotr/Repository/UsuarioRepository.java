package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    @Override
    Optional<UsuarioEntity> findById(Integer integer);

}
