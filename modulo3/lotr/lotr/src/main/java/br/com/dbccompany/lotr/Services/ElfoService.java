package br.com.dbccompany.lotr.Services;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ElfoService {
    @Autowired
    private ElfoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public ElfoDTO save( ElfoEntity e ){
        return this.saveUpdate(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public ElfoDTO update( ElfoEntity e, Integer id ){
        e.setId(id);
        return this.saveUpdate(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public ElfoDTO saveUpdate( ElfoEntity e ){
        return new ElfoDTO(repository.save( e ));
    }

    public ElfoDTO findById( Integer id ){
        Optional<ElfoEntity> e =  repository.findById(id);
        return e.isPresent() ? new ElfoDTO(e.get()) : null;
    }

    public List<ElfoDTO> findAll(){
        return parseList(repository.findAll());
    }

    public ElfoDTO findByExperiencia( Integer experiencia ){
        return new ElfoDTO(repository.findByExperiencia(experiencia));
    }

    public List<ElfoDTO> findAllByExperiencia( Integer experiencia ){
        return parseList(repository.findAllByExperiencia(experiencia));
    }

    public List<ElfoDTO> findAllByExperienciaIn( List<Integer> experiencias ){
        return parseList(repository.findAllByExperienciaIn(experiencias));
    }

    public ElfoDTO findByNome( String nome ){
        return new ElfoDTO(repository.findByNome(nome));
    }

    public List<ElfoDTO> findAllByNome( String nome ){
        return parseList(repository.findAllByNome(nome));
    }

    public List<ElfoDTO> findAllByNomeIn( List<String> nomes ){
        return parseList(repository.findAllByNomeIn(nomes));
    }

    public ElfoDTO findByQtdDano( Double dano ){
        return new ElfoDTO(repository.findByQtdDano(dano));
    }

    public List<ElfoDTO> findAllByQtdDano( Double dano ){
        return parseList(repository.findAllByQtdDano(dano));
    }

    public List<ElfoDTO> findAllByQtdDanoIn( List<Double> qtdsDano ){
        return parseList(repository.findAllByQtdDanoIn(qtdsDano));
    }

    public ElfoDTO findByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque){
        return new ElfoDTO(repository.findByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque));
    }

    public List<ElfoDTO> findAllByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque){
        return parseList(repository.findAllByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque));
    }

    public List<ElfoDTO> findAllByQtdExperienciaPorAtaqueIn( List<Integer> qtdsExperienciaPorAtaque ){
        return parseList(repository.findAllByQtdExperienciaPorAtaqueIn(qtdsExperienciaPorAtaque));
    }

    public ElfoDTO findByStatus( StatusEnum status ){
        return new ElfoDTO(repository.findByStatus(status));
    }

    public List<ElfoDTO> findAllByStatus( StatusEnum status ){
        return parseList(repository.findAllByStatus(status));
    }

    public List<ElfoDTO> findAllByStatusIn( List<StatusEnum> status ){
        return parseList(repository.findAllByStatusIn(status));
    }

    public ElfoDTO findByVida( Double vida ){
        return new ElfoDTO(repository.findByVida(vida));
    }

    public List<ElfoDTO> findAllByVida( Double vida ){
        return parseList(repository.findAllByVida(vida));
    }

    public List<ElfoDTO> findAllByVidaIn( List<Double> vidas ){
        return parseList(repository.findAllByVidaIn(vidas));
    }

    public ElfoDTO findByInventario( InventarioEntity inventario ){
        return new ElfoDTO(repository.findByInventario(inventario));
    }

    public List<ElfoDTO> findAllByInventario( InventarioEntity inventario ){
        return parseList(repository.findAllByInventario(inventario));
    }

    public List<ElfoDTO> findAllByInventarioIn( List<InventarioEntity> inventarios ){
        return parseList(repository.findAllByInventarioIn(inventarios));
    }

    public List<ElfoDTO> parseList(List<ElfoEntity> elfos ){
        if( elfos == null) return null;

        List<ElfoDTO> dto = new ArrayList<>();
        for( ElfoEntity elfo : elfos ){
            dto.add( new ElfoDTO( elfo ) );
        }
        return dto;
    }
}
