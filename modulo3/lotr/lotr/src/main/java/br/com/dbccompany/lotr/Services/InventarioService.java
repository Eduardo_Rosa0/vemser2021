package br.com.dbccompany.lotr.Services;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {
    @Autowired
    private InventarioRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO save(InventarioEntity inventario ){
        return this.saveUpdate(inventario);
    }

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO update( InventarioEntity inventario, Integer id ){
        inventario.setId(id);
        return this.saveUpdate(inventario);
    }

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO saveUpdate( InventarioEntity inventario ){
        return new InventarioDTO(repository.save( inventario ));
    }






















    public InventarioDTO findById( Integer id ){
        Optional<InventarioEntity> e =  repository.findById(id);
        return e.isPresent() ? new InventarioDTO(e.get()) : null;
    }

    public List<InventarioDTO> findAll(){
        return parseList(repository.findAll());
    }

    public InventarioDTO findByPersonagem( PersonagemEntity personagem ){
        return new InventarioDTO(repository.findByPersonagem(personagem));
    }

    public List<InventarioDTO> findAllByPersonagem( PersonagemEntity personagem ){
        return parseList(repository.findAllByPersonagem(personagem));
    }

    public List<InventarioDTO> findAllByPersonagemIn( List<PersonagemEntity> personagens ){
        return parseList(repository.findAllByPersonagemIn(personagens));
    }

    public InventarioDTO findByInventarioItemIn( List<Inventario_X_Item> inventarioItem ){
        return new InventarioDTO(repository.findByInventarioItemIn(inventarioItem));
    }

    public List<InventarioDTO> findAllByInventarioItemIn( List<Inventario_X_Item> inventarioItem ){
        return parseList(repository.findAllByInventarioItemIn(inventarioItem));
    }

    public List<InventarioDTO> parseList(List<InventarioEntity> inventarios){
        if( inventarios == null ) return null;

        List<InventarioDTO> dtos = new ArrayList<>();
        for(InventarioEntity inventario : inventarios){
            dtos.add(new InventarioDTO(inventario));
        }
        return dtos;
    }
}
