package br.com.dbccompany.lotr.Services;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class AnaoService{
    @Autowired
    private AnaoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public AnaoDTO save( AnaoEntity e ){
        return this.saveUpdate(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public AnaoDTO update( AnaoEntity e, Integer id ){
        e.setId(id);
        return this.saveUpdate(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public AnaoDTO saveUpdate( AnaoEntity e ){
        return new AnaoDTO(repository.save( e ));
    }

    public AnaoDTO findById(Integer id ){
        Optional<AnaoEntity> e =  repository.findById(id);
        return e.isPresent() ? new AnaoDTO(e.get()) : null;
    }

    public List<AnaoDTO> findAll(){
        return parseList(repository.findAll());
    }

    public AnaoDTO findByExperiencia( Integer experiencia ){
        return new AnaoDTO(repository.findByExperiencia(experiencia));
    }

    public List<AnaoDTO> findAllByExperiencia( Integer experiencia ){
        return  parseList(repository.findAllByExperiencia(experiencia));
    }

    public List<AnaoDTO> findAllByExperienciaIn( List<Integer> experiencias ){
        return parseList(repository.findAllByExperienciaIn(experiencias));
    }

    public AnaoDTO findByNome( String nome ){
        return new AnaoDTO(repository.findByNome(nome));
    }

    public List<AnaoDTO> findAllByNome( String nome ){
        return parseList(repository.findAllByNome(nome));
    }

    public List<AnaoDTO> findAllByNomeIn( List<String> nomes ){
        return parseList(repository.findAllByNomeIn(nomes));
    }
    public AnaoDTO findByQtdDano( Double dano ){
        return new AnaoDTO(repository.findByQtdDano(dano));
    }

    public List<AnaoDTO> findAllByQtdDano( Double dano ){
        return parseList(repository.findAllByQtdDano(dano));
    }

    public List<AnaoDTO> findAllByQtdDanoIn( List<Double> qtdsDano ){
        return parseList(repository.findAllByQtdDanoIn(qtdsDano));
    }

    public AnaoDTO findByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque){
        return new AnaoDTO(repository.findByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque));
    }

    public List<AnaoDTO> findAllByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque){
        return parseList(repository.findAllByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque));
    }

    public List<AnaoDTO> findAllByQtdExperienciaPorAtaqueIn( List<Integer> qtdsExperienciaPorAtaque ){
        return parseList(repository.findAllByQtdExperienciaPorAtaqueIn(qtdsExperienciaPorAtaque));
    }

    public AnaoDTO findByStatus( StatusEnum status ){
        return new AnaoDTO(repository.findByStatus(status));
    }

    public List<AnaoDTO> findAllByStatus( StatusEnum status ){
        return parseList(repository.findAllByStatus(status));
    }

    public List<AnaoDTO> findAllByStatusIn( List<StatusEnum> status ){
        return parseList(repository.findAllByStatusIn(status));
    }

    public AnaoDTO findByVida( Double vida ){
        return  new AnaoDTO(repository.findByVida(vida));
    }

    public List<AnaoDTO> findAllByVida( Double vida ){
        return parseList(repository.findAllByVida(vida));
    }

    public List<AnaoDTO> findAllByVidaIn( List<Double> vidas ){
        return parseList(repository.findAllByVidaIn(vidas));
    }

    public AnaoDTO findByInventario( InventarioEntity inventario ){
        return new AnaoDTO(repository.findByInventario(inventario));
    }

    public List<AnaoDTO> findAllByInventario( InventarioEntity inventario ){
        return parseList(repository.findAllByInventario(inventario));
    }

    public List<AnaoDTO> findAllByInventarioIn( List<InventarioEntity> inventarios ){
        return parseList(repository.findAllByInventarioIn(inventarios));
    }

    public List<AnaoDTO> parseList( List<AnaoEntity> anoes ){
        if( anoes == null) return null;

        List<AnaoDTO> dto = new ArrayList<>();
        for( AnaoEntity anao : anoes ){
            dto.add( new AnaoDTO( anao ) );
        }
        return dto;
    }
}
