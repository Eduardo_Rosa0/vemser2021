package br.com.dbccompany.lotr.Services;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService< R extends PersonagemRepository< E >, E extends PersonagemEntity>{

    @Autowired
    private R repository;

    @Transactional( rollbackFor = Exception.class)
    public E save( E e ){
        return this.saveUpdate(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public E update( E e, Integer id ){
        e.setId(id);
        return this.saveUpdate(e);
    }

    @Transactional( rollbackFor = Exception.class)
    public E saveUpdate( E e ){
        return repository.save( e );
    }

    public E findById( Integer id ){
        Optional<E> e =  repository.findById(id);
        return e.isPresent() ? e.get() : null;
    }

    public List<E> findAll(){
        return repository.findAll();
    }

    public E findByExperiencia( Integer experiencia ){
        return repository.findByExperiencia(experiencia);
    }

    public List<E> findAllByExperiencia( Integer experiencia ){
        return repository.findAllByExperiencia(experiencia);
    }

    public List<E> findAllByExperienciaIn( List<Integer> experiencias ){
        return repository.findAllByExperienciaIn(experiencias);
    }

    public E findByNome( String nome ){
        return repository.findByNome(nome);
    }

    public List<E> findAllByNome( String nome ){
        return repository.findAllByNome(nome);
    }

    public List<E> findAllByNomeIn( List<String> nomes ){
        return repository.findAllByNomeIn(nomes);
    }

    public E findByQtdDano( Double dano ){
        return repository.findByQtdDano(dano);
    }

    public List<E> findAllByQtdDano( Double dano ){
        return repository.findAllByQtdDano(dano);
    }

    public List<E> findAllByQtdDanoIn( List<Double> qtdsDano ){
        return repository.findAllByQtdDanoIn(qtdsDano);
    }

    public E findByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque){
        return repository.findByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    public List<E> findAllByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque){
        return repository.findAllByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    public List<E> findAllByQtdExperienciaPorAtaqueIn( List<Integer> qtdsExperienciaPorAtaque ){
        return repository.findAllByQtdExperienciaPorAtaqueIn(qtdsExperienciaPorAtaque);
    }

    public E findByStatus( StatusEnum status ){
        return repository.findByStatus(status);
    }

    public List<E> findAllByStatus( StatusEnum status ){
        return repository.findAllByStatus(status);
    }

    public List<E> findAllByStatusIn( List<StatusEnum> status ){
        return repository.findAllByStatusIn(status);
    }

    public E findByVida( Double vida ){
        return repository.findByVida(vida);
    }

    public List<E> findAllByVida( Double vida ){
        return repository.findAllByVida(vida);
    }

    public List<E> findAllByVidaIn( List<Double> vidas ){
        return repository.findAllByVidaIn(vidas);
    }

    public E findByInventario( InventarioEntity inventario ){
        return repository.findByInventario(inventario);
    }

    public List<E> findAllByInventario( InventarioEntity inventario ){
        return repository.findAllByInventario(inventario);
    }

    public List<E> findAllByInventarioIn( List<InventarioEntity> inventarios ){
        return repository.findAllByInventarioIn(inventarios);
    }
}
