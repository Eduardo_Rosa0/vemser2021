package br.com.dbccompany.lotr.Controller;


import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Services.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/inventario")
public class InventarioController {
    @Autowired
    private InventarioService service;

    @GetMapping( value = "/all")
    @ResponseBody
    public List<InventarioDTO> findAll(){
        return service.findAll();
    }

    @GetMapping( value = "/{ id }")
    @ResponseBody
    public InventarioDTO findById(@PathVariable Integer id){
        return service.findById(id);
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public InventarioDTO save( @RequestBody InventarioEntity inventario ){
        return service.save( inventario );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public InventarioDTO update( @RequestBody InventarioEntity inventario, @PathVariable  Integer id ){
        return service.update( inventario, id );
    }

    @PostMapping( value = "/personagem")
    @ResponseBody
    public InventarioDTO findByPersonagem( @RequestBody PersonagemEntity personagem ){
        return service.findByPersonagem(personagem);
    }

    @PostMapping( value = "/all/personagem")
    @ResponseBody
    public List<InventarioDTO> findAllByPersonagem( @RequestBody PersonagemEntity personagem ){
        return service.findAllByPersonagem(personagem);
    }

    @PostMapping( value = "/all/personagens")
    @ResponseBody
    public List<InventarioDTO> findAllByPersonagemIn( @RequestBody List<PersonagemEntity> personagens ){
        return service.findAllByPersonagemIn(personagens);
    }

    @PostMapping( value = "/inventario/item")
    @ResponseBody
    public InventarioDTO findByInventarioItemIn( @RequestBody List<Inventario_X_Item> inventarioItem ){
        return service.findByInventarioItemIn(inventarioItem);
    }

    @PostMapping( value = "/all/inventario/item")
    @ResponseBody
    public List<InventarioDTO> findAllByInventarioItemIn( @RequestBody List<Inventario_X_Item> inventarioItem ){
        return service.findAllByInventarioItemIn(inventarioItem);
    }
}
