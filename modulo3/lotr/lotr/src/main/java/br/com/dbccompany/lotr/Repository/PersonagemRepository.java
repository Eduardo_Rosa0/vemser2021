package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Primary
@Repository
public interface PersonagemRepository< T extends PersonagemEntity > extends CrudRepository<T, Integer> {
    List<T> findAll();

    Optional<T> findById( Integer id );
    List<T> findAllById( Integer id );
    List<T> findAllByIdIn( List<Integer> ids );

    T findByExperiencia( Integer experiencia );
    List<T> findAllByExperiencia( Integer experiencia );
    List<T> findAllByExperienciaIn( List<Integer> experiencias );

    T findByNome( String nome );
    List<T> findAllByNome( String nome );
    List<T> findAllByNomeIn( List<String> nomes );

    T findByQtdDano( Double dano );
    List<T> findAllByQtdDano( Double qtdDano );
    List<T> findAllByQtdDanoIn( List<Double> qtdsDano );

    T findByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque);
    List<T> findAllByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque );
    List<T> findAllByQtdExperienciaPorAtaqueIn( List<Integer> qtdsExperienciaPorAtaque );

    T findByStatus(StatusEnum status);
    List<T> findAllByStatus( StatusEnum status );
    List<T> findAllByStatusIn( List<StatusEnum> status );

    T findByVida( Double vida );
    List<T> findAllByVida( Double vida );
    List<T> findAllByVidaIn( List<Double> vidas );

    T findByInventario( InventarioEntity inventario );
    List<T> findAllByInventario( InventarioEntity inventario );
    List<T> findAllByInventarioIn( List<InventarioEntity> inventarios );
}
