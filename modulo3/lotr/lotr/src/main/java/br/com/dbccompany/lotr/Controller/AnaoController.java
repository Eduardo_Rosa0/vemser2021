package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Services.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/anao")
public class AnaoController {

    @Autowired
    private AnaoService service;

    @GetMapping( value = "/all")
    @ResponseBody
    public List<AnaoDTO> findAll(){
        return service.findAll();
    }

    @GetMapping( value = "/{ id }")
    @ResponseBody
    public AnaoDTO findById(@PathVariable Integer id){
        return service.findById(id);
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public AnaoDTO save(@RequestBody AnaoDTO anao ){
        return service.save( anao.converter() );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public AnaoDTO update( @RequestBody AnaoDTO anao, @PathVariable  Integer id ){
        return service.update( anao.converter(), id );
    }

    @GetMapping( value = "/experiencia/{ experiencia }")
    @ResponseBody
    public AnaoDTO findByExperiencia( @PathVariable Integer experiencia ){
        return  service.findByExperiencia(experiencia);
    }

    @GetMapping( value = "/all/experiencia/{ experiencia }")
    @ResponseBody
    public List<AnaoDTO> findAllByExperiencia( @PathVariable Integer experiencia ){
        return  service.findAllByExperiencia(experiencia);
    }

    @PostMapping( value = "/all/experiencias")
    @ResponseBody
    public List<AnaoDTO> findAllByExperienciaIn( @RequestBody List<Integer> experiencias ){
        return  service.findAllByExperienciaIn(experiencias);
    }

    @GetMapping( value = "/nome/{ nome }")
    @ResponseBody
    public AnaoDTO findByNome( @PathVariable String nome ){
        return  service.findByNome(nome);
    }

    @GetMapping( value = "/all/nome/{ nome }")
    @ResponseBody
    public List<AnaoDTO> findAllByNome( @PathVariable String nome ){
        return  service.findAllByNome(nome);
    }

    @PostMapping( value = "/all/nomes")
    @ResponseBody
    public List<AnaoDTO> findAllByNomeIn( @RequestBody List<String> nomes ){
        return  service.findAllByNomeIn(nomes);
    }

    @PostMapping( value = "/qtd/dano")
    @ResponseBody
    public AnaoDTO findByQtdDano( @RequestBody Double dano ){
        return  service.findByQtdDano(dano);
    }

    @PostMapping( value = "/all/qtd/dano")
    @ResponseBody
    public List<AnaoDTO> findAllByQtdDano( @RequestBody Double dano ){
        return  service.findAllByQtdDano(dano);
    }

    @PostMapping( value = "/all/qtds/dano")
    @ResponseBody
    public List<AnaoDTO> findAllByQtdDanoIn( @RequestBody List<Double> qtdsDano ){
        return  service.findAllByQtdDanoIn(qtdsDano);
    }

    @GetMapping( value = "/qtd/xp/ataque/{ qtdExperienciaPorAtaque }")
    @ResponseBody
    public AnaoDTO findByQtdExperienciaPorAtaque( @PathVariable Integer qtdExperienciaPorAtaque ){
        return  service.findByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    @GetMapping( value = "/all/qtd/xp/ataque/{ qtdExperienciaPorAtaque }")
    @ResponseBody
    public List<AnaoDTO> findAllByQtdExperienciaPorAtaque( @PathVariable Integer qtdExperienciaPorAtaque ){
        return  service.findAllByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    @PostMapping( value = "/all/qtds/xp/ataque")
    @ResponseBody
    public List<AnaoDTO> findAllByQtdExperienciaPorAtaqueIn( @RequestBody List<Integer> qtdsExperienciaPorAtaque ){
        return  service.findAllByQtdExperienciaPorAtaqueIn(qtdsExperienciaPorAtaque);
    }

    @PostMapping( value = "/status")
    @ResponseBody
    public AnaoDTO findByStatus( @RequestBody StatusEnum status ){
        return  service.findByStatus(status);
    }

    @PostMapping( value = "/all/status")
    @ResponseBody
    public List<AnaoDTO> findAllByStatus( @RequestBody StatusEnum status ){
        return  service.findAllByStatus(status);
    }

    @PostMapping( value = "/all/status/list")
    @ResponseBody
    public List<AnaoDTO> findAllByStatusIn( @RequestBody List<StatusEnum> status ){
        return  service.findAllByStatusIn(status);
    }

    @PostMapping( value = "/vida")
    @ResponseBody
    public AnaoDTO findByVida( @RequestBody Double vida ){
        return  service.findByVida(vida);
    }

    @PostMapping( value = "/all/vida")
    @ResponseBody
    public List<AnaoDTO> findAllByVida( @RequestBody Double vida ){
        return  service.findAllByVida(vida);
    }

    @PostMapping( value = "/all/vidas")
    @ResponseBody
    public List<AnaoDTO> findAllByVidaIn( @RequestBody List<Double> vidas ){
        return  service.findAllByVidaIn(vidas);
    }

    @PostMapping( value = "/inventario")
    @ResponseBody
    public AnaoDTO findByInventario( @RequestBody InventarioEntity inventario ){
        return  service.findByInventario(inventario);
    }

    @PostMapping( value = "/all/inventario")
    @ResponseBody
    public List<AnaoDTO>  findAllByInventario( @RequestBody InventarioEntity inventario ){
        return  service.findAllByInventario(inventario);
    }

    @PostMapping( value = "/all/inventarios")
    @ResponseBody
    public List<AnaoDTO>  findAllByInventarioIn( @RequestBody List<InventarioEntity> inventarios){
        return  service.findAllByInventarioIn(inventarios);
    }
}
