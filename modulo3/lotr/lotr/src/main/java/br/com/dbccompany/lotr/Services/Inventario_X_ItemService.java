package br.com.dbccompany.lotr.Services;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.*;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService {
    @Autowired
    private Inventario_X_ItemRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO save(Inventario_X_Item inventario_x_item ){
        return this.saveUpdate(inventario_x_item);
    }

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO update( Inventario_X_Item inventario_x_item, Inventario_X_ItemId id ){
        inventario_x_item.setId(id);
        return this.saveUpdate(inventario_x_item);
    }

    @Transactional( rollbackFor = Exception.class)
    public Inventario_X_ItemDTO saveUpdate( Inventario_X_Item inventario_x_item ){
        return new Inventario_X_ItemDTO(repository.save( inventario_x_item ));
    }

    public Inventario_X_ItemDTO findById(Inventario_X_ItemId id){
        Optional<Inventario_X_Item> e =  repository.findById(id);
        return e.isPresent() ? new Inventario_X_ItemDTO(e.get()) : null;
    }

    public List<Inventario_X_ItemDTO> findAll(){
        return  parseList(repository.findAll());
    }

    public Inventario_X_ItemDTO findByInventario( InventarioEntity inventario){
        return new Inventario_X_ItemDTO(repository.findByInventario(inventario));
    }

    public List<Inventario_X_ItemDTO> findAllByInventario( InventarioEntity inventario){
        return parseList(repository.findAllByInventario(inventario));
    }

    public List<Inventario_X_ItemDTO> findAllByInventarioIn( List<InventarioEntity> inventarios){
        return parseList(repository.findAllByInventarioIn(inventarios));
    }

    public Inventario_X_ItemDTO findByItem( ItemEntity item ){
        return new Inventario_X_ItemDTO(repository.findByItem(item));
    }

    public List<Inventario_X_ItemDTO> findAllByItem( ItemEntity item ){
        return parseList(repository.findAllByItem(item));
    }

    public List<Inventario_X_ItemDTO> findAllByItemIn( List<ItemEntity> itens ){
        return parseList(repository.findAllByItemIn(itens));
    }

    public Inventario_X_ItemDTO findByQuantidade( Integer quantidade ){
        return new Inventario_X_ItemDTO(repository.findByQuantidade(quantidade));
    }

    public List<Inventario_X_ItemDTO> findAllByQuantidade( Integer quantidade ){
        return parseList(repository.findAllByQuantidade(quantidade));
    }

    public List<Inventario_X_ItemDTO> findAllByQuantidadeIn( List<Integer> quantidades ){
        return parseList(repository.findAllByQuantidadeIn(quantidades));
    }

    public List<Inventario_X_ItemDTO> parseList( List<Inventario_X_Item> i_X_i){
        if( i_X_i == null) return null;

        List<Inventario_X_ItemDTO> dto = new ArrayList<>();
        for( Inventario_X_Item i : i_X_i ){
            dto.add( new Inventario_X_ItemDTO( i ) );
        }
        return dto;

    }
}
