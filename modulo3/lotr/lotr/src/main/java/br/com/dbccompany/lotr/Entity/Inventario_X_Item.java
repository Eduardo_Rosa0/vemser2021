package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
public class Inventario_X_Item {
    @EmbeddedId
    private Inventario_X_ItemId id;


    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("id_inventario")
    private InventarioEntity inventario;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("id_item")
    private ItemEntity item;

    private Integer quantidade;


    public Inventario_X_ItemId getId() {
        return id;
    }

    public void setId(Inventario_X_ItemId id) {
        this.id = id;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public ItemEntity getItem() {
        return item;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}