package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Services.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "api/elfo")
public class ElfoController {

    @Autowired
    private ElfoService service;

    @GetMapping( value = "/all")
    @ResponseBody
    public List<ElfoDTO> findAll(){
        return service.findAll();
    }

    @GetMapping( value = "/{ id }")
    @ResponseBody
    public ElfoDTO findById(@PathVariable Integer id){
        return service.findById(id);
    }


    @PostMapping( value = "/salvar")
    @ResponseBody
    public ElfoDTO save(@RequestBody ElfoEntity elfo ){
        return service.save( elfo );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public ElfoDTO update( @RequestBody ElfoEntity elfo, @PathVariable  Integer id ){
        return service.update( elfo, id );
    }

    @GetMapping( value = "/experiencia/{ experiencia }")
    @ResponseBody
    public ElfoDTO findByExperiencia( @PathVariable Integer experiencia ){
        return  service.findByExperiencia(experiencia);
    }

    @GetMapping( value = "/all/experiencia/{ experiencia }")
    @ResponseBody
    public List<ElfoDTO> findAllByExperiencia( @PathVariable Integer experiencia ){
        return  service.findAllByExperiencia(experiencia);
    }

    @PostMapping( value = "/all/experiencias")
    @ResponseBody
    public List<ElfoDTO> findAllByExperienciaIn( @RequestBody List<Integer> experiencias ){
        return  service.findAllByExperienciaIn(experiencias);
    }

    @GetMapping( value = "/nome/{ nome }")
    @ResponseBody
    public ElfoDTO findByNome( @PathVariable String nome ){
        return  service.findByNome(nome);
    }

    @GetMapping( value = "/all/nome/{ nome }")
    @ResponseBody
    public List<ElfoDTO> findAllByNome( @PathVariable String nome ){
        return  service.findAllByNome(nome);
    }

    @PostMapping( value = "/all/nomes")
    @ResponseBody
    public List<ElfoDTO> findAllByNomeIn( @RequestBody List<String> nomes ){
        return  service.findAllByNomeIn(nomes);
    }

    @PostMapping( value = "/qtd/dano")
    @ResponseBody
    public ElfoDTO findByQtdDano( @RequestBody Double dano ){
        return  service.findByQtdDano(dano);
    }

    @PostMapping( value = "/all/qtd/dano")
    @ResponseBody
    public List<ElfoDTO> findAllByQtdDano( @RequestBody Double dano ){
        return  service.findAllByQtdDano(dano);
    }

    @PostMapping( value = "/all/qtds/dano")
    @ResponseBody
    public List<ElfoDTO> findAllByQtdDanoIn( @RequestBody List<Double> qtdsDano ){
        return  service.findAllByQtdDanoIn(qtdsDano);
    }

    @GetMapping( value = "/qtd/xp/ataque/{ qtdExperienciaPorAtaque }")
    @ResponseBody
    public ElfoDTO findByQtdExperienciaPorAtaque( @PathVariable Integer qtdExperienciaPorAtaque ){
        return  service.findByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    @GetMapping( value = "/all/qtd/xp/ataque/{ qtdExperienciaPorAtaque }")
    @ResponseBody
    public List<ElfoDTO> findAllByQtdExperienciaPorAtaque( @PathVariable Integer qtdExperienciaPorAtaque ){
        return  service.findAllByQtdExperienciaPorAtaque(qtdExperienciaPorAtaque);
    }

    @PostMapping( value = "/all/qtds/xp/ataque")
    @ResponseBody
    public List<ElfoDTO> findAllByQtdExperienciaPorAtaqueIn( @RequestBody List<Integer> qtdsExperienciaPorAtaque ){
        return  service.findAllByQtdExperienciaPorAtaqueIn(qtdsExperienciaPorAtaque);
    }

    @PostMapping( value = "/status")
    @ResponseBody
    public ElfoDTO findByStatus( @RequestBody StatusEnum status ){
        return  service.findByStatus(status);
    }

    @PostMapping( value = "/all/status")
    @ResponseBody
    public List<ElfoDTO> findAllByStatus( @RequestBody StatusEnum status ){
        return  service.findAllByStatus(status);
    }

    @PostMapping( value = "/all/status/list")
    @ResponseBody
    public List<ElfoDTO> findAllByStatusIn( @RequestBody List<StatusEnum> status ){
        return  service.findAllByStatusIn(status);
    }

    @PostMapping( value = "/vida")
    @ResponseBody
    public ElfoDTO findByVida( @RequestBody Double vida ){
        return  service.findByVida(vida);
    }

    @PostMapping( value = "/all/vida")
    @ResponseBody
    public List<ElfoDTO> findAllByVida( @RequestBody Double vida ){
        return  service.findAllByVida(vida);
    }

    @PostMapping( value = "/all/vidas")
    @ResponseBody
    public List<ElfoDTO> findAllByVidaIn( @RequestBody List<Double> vidas ){
        return  service.findAllByVidaIn(vidas);
    }

    @PostMapping( value = "/inventario")
    @ResponseBody
    public ElfoDTO findByInventario( @RequestBody InventarioEntity inventario ){
        return  service.findByInventario(inventario);
    }

    @PostMapping( value = "/all/inventario")
    @ResponseBody
    public List<ElfoDTO>  findAllByInventario( @RequestBody InventarioEntity inventario ){
        return  service.findAllByInventario(inventario);
    }

    @PostMapping( value = "/all/inventarios")
    @ResponseBody
    public List<ElfoDTO>  findAllByInventarioIn( @RequestBody List<InventarioEntity> inventarios){
        return  service.findAllByInventarioIn(inventarios);
    }

}





















