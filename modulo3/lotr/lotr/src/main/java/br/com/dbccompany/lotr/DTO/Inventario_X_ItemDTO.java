package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;

public class Inventario_X_ItemDTO {
    private Integer quantidade;
    private InventarioEntity inventario;
    private ItemEntity item;

    public Inventario_X_ItemDTO(Inventario_X_Item inventario_x_item){
        this.quantidade = inventario_x_item.getQuantidade();
        this.inventario = inventario_x_item.getInventario();
        this.item = inventario_x_item.getItem();
    }

    public Inventario_X_Item converter(){
        Inventario_X_Item inventario = new Inventario_X_Item();
        inventario.setQuantidade(this.quantidade);
        inventario.setInventario(this.inventario);
        inventario.setItem(this.item);
        //  solução implementada em aula
        /*Inventario_X_Item inventarioItem = new Inventario_X_Item();
        Inventario_X_ItemId id = new Inventario_X_ItemId(this.idInventario, this.idItem);
        InventarioEntity inventario = new InventarioEntity();
        inventario.setId(this.idInventario);
        ItemEntity item = new ItemEntity();
        inventarioItem.setId(id);
        inventarioItem.setQuantidade(this.quantidade);
        inventarioItem.setInventario(inventario);
        inventarioItem.setItem(item);
        */
        return inventario;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public ItemEntity getItem() {
        return item;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }
}
