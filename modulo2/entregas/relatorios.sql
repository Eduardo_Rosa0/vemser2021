-- primeiro relatório;
SELECT DISTINCT
    banco.nome,
    pais.nome
FROM
    banco
    INNER JOIN agencia ON agencia.id_banco = banco.codigo
    INNER JOIN endereco ON endereco.id_endereco = agencia.id_endereco
    INNER JOIN cidade ON endereco.id_cidade = cidade.id_cidade
    INNER JOIN estado ON cidade.id_estado = estado.id_estado
    INNER JOIN pais ON estado.id_pais = pais.id_pais;
    
--

-- segundo relatório;

SELECT
    agencia.*,
    conta.*,
    cliente.*,
    endereco.*
FROM
    agencia
    INNER JOIN conta ON agencia.codigo = conta.codigo_agencia
    INNER JOIN cliente_x_conta ON cliente_x_conta.codigo = conta.codigo
    INNER JOIN cliente_x_endereco ON cliente_x_endereco.id_cliente = cliente_x_conta.id_cliente
    INNER JOIN endereco ON cliente_x_endereco.id_endereco = endereco.id_endereco
    INNER JOIN cliente ON cliente_x_endereco.id_cliente = cliente.cpf;

--

-- terceiro relatório;

SELECT
    cliente.*,
    endereco.*
FROM
    cliente
    INNER JOIN cliente_x_endereco ON cliente_x_endereco.id_cliente = cliente.cpf
    INNER JOIN endereco ON endereco.id_endereco = cliente_x_endereco.id_endereco;

