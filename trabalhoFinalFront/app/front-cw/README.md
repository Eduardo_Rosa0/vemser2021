# Checklist trabalho

> login
- [X]  tela de login;
  - publica;
  - nenhum link;
  - redirecionamento somente pra home;
---
> usuario
- [X] tela de cadastro de usuario;
  - publica;
  - links para:
    - login 
  - redirecionamentos para:
    - login
  
- [ ] tela de edição de usuario;
  - privada;
  - links para:
    - home
    - lista de usuarios;
  - redirecionamento:
    - lista de usuarios
  
- [ ] tela de exibição de usuarios;
  - privada;
  - links para:
    - home
  - redirecionamento para:
    - lista de usuarios
---
> tipo de contato
- [ ] tela de cadastro de tipos de contato;
  - privada;
  - link para:
    - home
    - lista de contatos
    - lista de clientes
  - redirecionamentos para:
    - lista de contatos;
- [ ] tela de edição de tipos de contato
  - privada;
  - link para:
    - home
    - lista de contatos
  - redirecionamentos para:
    - lista de contatos;
---
> contato
- [ ] tela de edicao de contato
  - privada;
  - link para:
    - home;
    - lista de contato;
  - redirecionamentos para:
    - lista de contatos;
---


