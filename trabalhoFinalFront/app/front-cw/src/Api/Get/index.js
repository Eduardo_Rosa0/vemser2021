import axios from "axios";

import Urls from '../../Constants/Url';

export default class Get {
  constructor() {
    this.url = Urls.CW;
    this.token = localStorage.getItem('token');
    this.config = { headers: { Authorization: this.token } };
  }

  async buscarUsuarioPorId(id){
    const response = await axios.get(
      `${this.url}/usuario/${id}`,
      this.config);
      return response.data;
  }

  async buscarUsuarios(){
    const response = await axios.get(
      `${this.url}/usuario/`,
      this.config);
      return response.data;
  }

  async buscarTiposContato() {
    const response = await axios.get(
      `${this.url}/tipos/contato/`,
      this.config);
    return response.data;
  }

  async buscarClientes() {
    const response = await axios.get(
      `${this.url}/cliente/`,
      this.config);
    return response.data;
  }
}