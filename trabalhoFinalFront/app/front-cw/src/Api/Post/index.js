import axios from "axios";

import Urls from '../../Constants/Url';

export default class Post {
  constructor() {
    this.url = Urls.CW;
    this.token = localStorage.getItem('token');
    this.config = { headers: { Authorization: this.token}};
  }

  async salvarUsuario( usuario ){
    const response = await axios.post(
      `${ this.url }/usuario/salvar`,
      usuario)
    return response.data;
  }

  async salvarTipoContato(tipoContato) {
    const response = await axios.post(
      `${this.url}/tipoContato/salvar`,
      tipoContato,
      this.config)
    return response.data;
  }

  async salvarCliente(cliente) {
    const response = await axios.post(
      `${this.url}/cliente/salvar`,
      cliente,
      this.config)
    return response.data;
  }
  async salvarContato(contato) {
    const response = await axios.post(
      `${this.url}/contato/salvar`,
      contato,
      this.config)
    return response.data;
  }

}