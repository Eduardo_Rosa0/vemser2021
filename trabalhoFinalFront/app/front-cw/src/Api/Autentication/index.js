import axios from 'axios';

import Urls from '../../Constants/Url';

export default class Authentication {
  constructor() {
    this.url = Urls.DEFAULT;
  }

  async cadastrar(params) {
    try {

      const response = await axios.post(`${this.url}/cadastrar`,
        {
          nome: params.nome,
          email: params.email,
          login: params.login,
          senha: params.senha
        });

      return response
    } catch (error) {
      console.error('Authentication > cadastrar');
      console.error(error);
    }
  }

  async logar(params) {
      const response = await axios.post(`${this.url}/login`,
        {
          login: params.login,
          senha: params.senha
        });
      return response;
  }

}