import axios from "axios";

import Urls from '../../Constants/Url';

export default class Put {
  constructor() {
    this.url = Urls.CW;
    this.token = localStorage.getItem('token');
    this.config = { headers: { Authorization: this.token}};
  }

  async editarUsuario(usuario) {
    try {
      const response = await axios.put(
        `${this.url}/usuario/editar/${usuario.id}`,
        usuario,
        this.config);
      return response.data;
    } catch (error) {
      console.log(error);
    }
  }

  async editarTipoContato(tipoContato, id) {
    const response = await axios.put(
      `${this.url}/tipoContato/editar/${id}`,
      tipoContato,
      this.config);
    return response.data;
  }

  async editarCliente(cliente, id) {
    const response = await axios.put(
      `${this.url}/cliente/editar/${id}`,
      cliente,
      this.config);
    return response.data;
  }

}