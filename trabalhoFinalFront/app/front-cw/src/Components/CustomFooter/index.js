import React, { Component } from 'react';

import Layout from "../../Middleware/Layout";

const { Footer } = Layout;

export default class CustomFooter extends Component{
  render(){
    return (
      <Footer className="footer fundo-claro"></Footer>
    )
  }
}
