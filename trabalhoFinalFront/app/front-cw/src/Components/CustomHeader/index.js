import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Path from '../../Constants/Path';
import Layout from "../../Middleware/Layout";

const { Header } = Layout;

export default class CustomHeader extends Component {
  render() {
    return (
      <Header className="header texto-centralizado" >
        <Link to={Path.Home}>
          <h1 className="texto-negrito">{'Página inicial'}</h1>
        </Link>
      </Header>
    );
  }
};