import React, { Component } from "react";

export default class CustomForm extends Component {

  render() {
    const { onChange, onSubmit, inputs, titlePage, id } = this.props;
    return (
      <React.Fragment>

        <section className="texto-centralizado">
          <p className="title-form">{titlePage}</p>
        </section>
        <section className="container-form">
          <form className="form" onSubmit={e => onSubmit(e)}>
            {
              id && (<input name="id" type="hidden" value={id}></input>)
            }
            {
              inputs.map((input, indice) => {

                return (
                  <section className="flex-container-column" key={indice}>
                    <label className="label" htmlFor={input.name}>{input.textoLabel}</label>
                    <input
                      className="input texto-centralizado"
                      onChange={e => onChange(e)}
                      name={input.name}
                      id={input.id}
                      required={input.required ? input.required : ""}
                      type={input.type}
                      placeholder={input.placeholder}></input>
                  </section>
                )
              })
            }

            <section className="flex-container-row conteiner-btns">
              <button className="btn-reset texto-escuro" type="reset" > Limpar </button>
              <button className="btn-submit texto-escuro" type="submit" > Enviar </button>
            </section>
          </form>
        </section>
      </React.Fragment>
    );
  }
}