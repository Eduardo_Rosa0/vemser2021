import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Menu from "../../Middleware/Menu";
import Path from '../../Constants/Path';
import Layout from "../../Middleware/Layout";

const { Sider } = Layout;
const { SubMenu } = Menu;

export default class CustomSider extends Component {

  deslogar() {
    localStorage.removeItem('token')
    localStorage.removeItem('login');
    this.setState(state => { return { ...state, usuarioLogado: false } });
    window.location.href = Path.Login;
  }

  render() {

    return (
      <Sider className="sider">
        <section>
          {
            localStorage.getItem('token')
              ? (
                <Menu theme="dark">
                  <Menu.Item className='link-destacado' key="1">
                    <Link to={Path.Home}> Página inicial </Link>
                  </Menu.Item>

                  <SubMenu
                    className="link-normal"
                    key="sub1"
                    title="Usuário"
                  >
                    <Menu.Item key="2">
                      <Link to={Path.Cadastro.usuario}> Cadastrar Novo </Link>
                    </Menu.Item>

                    <Menu.Item key="3">
                      <Link to={Path.Usuarios}> Listagem com todos </Link>
                    </Menu.Item>
                  </SubMenu>

                  <SubMenu
                    key="sub5"
                    title="Tipo de Contato"
                    className="link-normal"
                  >
                    <Menu.Item key="10">
                      <Link to={Path.Home} > Cadastrar Novo </Link>
                    </Menu.Item>
                    <Menu.Item key="11">
                      <Link to={Path.Home} > Listagem com todos </Link>
                    </Menu.Item>
                  </SubMenu>
                  <Menu.Item className='link-destacado' key="12">
                    <Link to="" onClick={this.deslogar.bind(this)}> Deslogar </Link>
                  </Menu.Item>

                </Menu>
              )
              : (
                <Menu theme="dark">
                  <Menu.Item className='link-destacado' key="1">
                    <Link to={Path.Home}> Página inicial </Link>
                  </Menu.Item>
                  <Menu.Item className="link-normal" key="2">
                    <Link to={Path.Cadastro.usuario}> Cadastrar Novo Usuário </Link>
                  </Menu.Item>
                  <Menu.Item className='link-destacado' key="3">
                    <Link to={Path.Login}> Fazer Login </Link>
                  </Menu.Item>
                </Menu>
              )
          }

        </section>
      </Sider>
    );
  }
}