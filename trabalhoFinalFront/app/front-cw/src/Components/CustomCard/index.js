import React, { Component } from "react";
import { Link } from 'react-router-dom';

import Card from '../../Middleware/Card';

export default class CustomCard extends Component {
  render() {
    const {  title, content, link } = this.props;
    return (
      <React.Fragment>
        {
          link
            ? (
              <Card
                id="card"
                size="small"
                title={title}
                extra={<Link to={link} >Editar</Link>}
              >
                {content}
              </Card>
            )
            : (
              <Card
                id="card"
                size="small"
                title={title}
              >
                {content}
              </Card>
            )
        }
      </React.Fragment>
    )
  }
}