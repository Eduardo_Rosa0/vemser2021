const inputs = () => {
  return [
    {
      textoLabel: '* Login',
      name: 'login',
      id: 'login',
      type: 'text',
      placeholder: '[ Login do usuário ]'
    },
    {
      textoLabel: '* Senha',
      name: 'senha',
      id: 'senha',
      type: 'password',
      placeholder: '[ ***** ]'
    },
  ]
}

export default inputs;