const inputs = () => {
  return [
    {
      textoLabel: '* Nome',
      name: 'nome',
      id: 'nome',
      type: 'text',
      placeholder: '[ Nome completo ]'
    },
    {
      textoLabel: '* E-mail',
      name: 'email',
      id: 'email',
      type: 'email',
      placeholder: '[ **@**.com ]'
    },
    {
      textoLabel: '* Login',
      name: 'login',
      id: 'login',
      type: 'text',
      placeholder: '[ Login do usuário ]'
    },
    {
      textoLabel: '* Senha',
      name: 'senha',
      id: 'senha',
      type: 'password',
      placeholder: '[ ***** ]'
    },
  ]
}

export default inputs;