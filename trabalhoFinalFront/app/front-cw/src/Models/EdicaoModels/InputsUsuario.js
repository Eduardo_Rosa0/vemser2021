const inputs = ({ nome, email, login, senha }) => {
  return [
    {
      textoLabel: '* Nome',
      name: 'nome',
      id: 'nome',
      type: 'text',
      required: true,
      valorAtual: nome ? nome : '',
      placeholder: nome ? nome : '[ Nome completo ]'
    },
    {
      textoLabel: '* E-mail',
      name: 'email',
      id: 'email',
      type: 'email',
      required: true,
      valorAtual: email ? email : '',
      placeholder: email ? email : '[ **@**.com ]'
    },
    {
      textoLabel: '* Login',
      name: 'login',
      id: 'login',
      type: 'text',
      required: true,
      valorAtual: login ? login : '',
      placeholder: login ? login : '[ Login do usuário ]'
    },
    {
      textoLabel: 'Senha',
      name: 'senha',
      id: 'senha',
      type: 'password',
      valorAtual: senha ? senha : '',
      placeholder: senha ? senha : '[ ***** ]'
    }
  ]
}

export default inputs;