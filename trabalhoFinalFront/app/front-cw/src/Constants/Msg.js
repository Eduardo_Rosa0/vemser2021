const MsgsEdicao = {
  sucesso: 'Atualização bem sucedida',
  aviso: 'Verifique os dados e tente novamente',
  erro: 'Erro ao atualizar'
}

const MsgsCadastro = {
  sucesso: 'Cadastro bem sucedido',
  aviso: 'Verifique os dados e tente novamente',
  erro: 'Erro ao cadastrar'
}

const MsgsBusca = {
  msg: 'Nenhum item encontrado, tente cadastrar um novo'
}

const MsgsLogin = {
  sucesso: 'Login bem sucedido, você será redirecionado em instantes',
  erro: 'Verifique os dados e tente novamente'
}

export { MsgsEdicao, MsgsCadastro, MsgsBusca, MsgsLogin };