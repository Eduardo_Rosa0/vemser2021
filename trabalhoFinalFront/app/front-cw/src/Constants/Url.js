const urls = {
  DEFAULT: 'http://localhost:8080',
  CW: 'http://localhost:8080/api/cw'
};

export default urls;