const path = {
  Home: '/',
  Login: '/login',
  Usuarios: '/usuarios',
  Cadastro: {
    usuario: '/cadastro/usuario',
    cliente: '/cadastro/cliente',
    contato: '/cadastro/contato',
    tipoContato: '/cadastro/tipo/contato'
  },
  Edicao: {
    usuario: '/edicao/usuario/:id',
  },
  Clientes: '/clientes',
  Contatos: '/contatos',
  Contratacoes: '/contratacoes',
  Espacos: '/espacos',
};

export default path;