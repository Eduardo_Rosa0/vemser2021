import React, { Component } from 'react';

import Layout from "../../Middleware/Layout";
import CustomHeader from "../../Components/CustomHeader";
import CustomSider from "../../Components/CustomSider";
import CustomFooter from "../../Components/CustomFooter";

const { Content } = Layout;

export default class Home extends Component {
  render() {
    return (
        <Layout>
          <CustomHeader />
          <Layout>
            <CustomSider />
            <Content className="content" >
              <section className="home" >
                <div>
                  <h1>Olá</h1>
                </div>
              </section>
            </Content>
          </Layout>
          <CustomFooter />
        </Layout>
    )
  }
}