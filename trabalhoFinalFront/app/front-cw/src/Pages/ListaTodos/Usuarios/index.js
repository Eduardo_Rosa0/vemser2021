import React, { Component } from 'react';

import Path from '../../../Constants/Path';
import { MsgsBusca } from '../../../Constants/Msg';
import GetMethodApi from '../../../Api/Get';
import Layout from "../../../Middleware/Layout";
import CustomHeader from '../../../Components/CustomHeader';
import CustomSider from '../../../Components/CustomSider';
import CustomFooter from '../../../Components/CustomFooter';
import CustomCard from '../../../Components/CustomCard';

const { Content } = Layout;

export default class Usuarios extends Component {
  constructor(props) {
    super(props);
    this.get = new GetMethodApi();
    this.mensagemListaVazia = MsgsBusca.msg;
    this.state = {
      todos: [],
      buscados: []
    }
  }

  filtar(event) {
    if (!event.target.value) {
      const { todos } = this.state
      this.setState(state => {
        return {
          ...state,
          buscados: todos.filter(usr => usr.id <= 4)
        }
      })
    }

    if ([...event.target.value][0] === '#' && event.target.value.length > 1) {
      const porLogin = this.state.todos.filter(usuario => {
        return usuario.login
          .includes(event.target.value
            .replace('#', '')
          )
      });

      this.setState(state => {
        return {
          ...state,
          buscados: porLogin
        }
      })

    } else {
      const porNome = this.state.todos.filter(usuario => {
        return usuario.nome
          .toUpperCase()
          .includes(event.target.value
            .toUpperCase()
          )
      });

      this.setState(state => {
        return {
          ...state,
          buscados: porNome
        }
      })
    }
  }

  async buscarUsuarios() {
    const usuarios = await this.get.buscarUsuarios();
    return usuarios;
  }

  componentDidMount() {
    try {
      this.buscarUsuarios().then(usuarios => {
        this.setState(state => {
          return {
            ...state,
            todos: usuarios,
            buscados: usuarios.filter(usr => usr.id <= 4)
          }
        })
      })

    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { buscados, todos } = this.state;

    return (
      <Layout>
        <CustomHeader />
        <Layout>
          <CustomSider />
          <Content className="content texto-centralizado fundo-escuro" >
            {
              todos.length > 0
                ? (
                  <section className="">
                    <h2 className="title-listagem">Usuários cadastrados</h2>
                    <section className="input-search-section">
                      <input type="text" className="input-search" placeholder={`Buscar por nome`} onChange={e => this.filtar(e)}></input>
                      <p>Voce também pode buscar pelo codigo de usuario com '#'</p>
                      <p>Ex.: #99245</p>
                    </section>
                    <section className="cards-container">
                      {
                        buscados
                          .map((usuario, indice) => {
                            return (
                              <CustomCard
                                key={indice}
                                title={usuario.nome}
                                link={`${Path.Edicao.usuario.replace(':id', usuario.id)}`}
                                content={
                                  <div className="content-card">
                                    <p><span>E-mail: </span>{usuario.email} </p>
                                    <p><span>Código de login:</span> {usuario.login} </p>
                                  </div>
                                }
                              />
                            )
                          })
                      }

                    </section>
                  </section>
                )
                : (<h3 className="title-listagem">{this.mensagemListaVazia}</h3>)
            }
          </Content>
        </Layout>

        <CustomFooter />
      </Layout>
    )
  }
}