import React, { Component } from "react";

import Alert from '../../../Middleware/Alert';
import { MsgsEdicao } from '../../../Constants/Msg';
import GetMethodApi from '../../../Api/Get';
import PutMethodApi from '../../../Api/Put';
import Layout from "../../../Middleware/Layout";
import CustomForm from "../../../Components/CustomForm";
import CustomHeader from "../../../Components/CustomHeader";
import CustomSider from "../../../Components/CustomSider";
import CustomFooter from "../../../Components/CustomFooter";
import Path from "../../../Constants/Path";
import inputModel from '../../../Models/EdicaoModels/InputsUsuario'

const { Content } = Layout;

export default class CadastroUsuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      erro: false,
      sucesso: false,
      inputs: []
    }
    this.id = this.props.match.params.id;
    this.get = new GetMethodApi();
    this.put = new PutMethodApi();
  }


  alert(type) {
    if (type === 'erro') {
      this.setState(state => { return { ...state, erro: true } })
      setTimeout(() => {
        this.setState(state => { return { ...state, erro: false } })
      }, 2000);
    }
    if (type === 'sucesso') {
      this.setState(state => { return { ...state, sucesso: true } })
      setTimeout(() => {
        this.setState(state => { return { ...state, sucesso: false } });
        window.location.href = Path.Usuarios;
      }, 2000)
    }
  }


  componentDidMount() {
    this.get.buscarUsuarioPorId(this.id)
      .then(response => {

        const { nome, email, login, senha } = response;
        this.setState(state => {
          return {
            ...state,
            nome,
            email,
            login,
            senha,
            inputs: inputModel(
              {
                nome: nome, 
                email: email, 
                login: login, 
                senha: senha 
              }
            )
          }
        })
      })
      .catch(error => console.error(error));
  }

  onChange(event) {
    
    const valorSalvo = this.state.inputs.find(input => {
      return input.name === event.target.name;
    }).valorAtual;

    this.setState(state => {
      return {
        ...state,
        [event.target.name]: event.target.value
          ? event.target.value
          : valorSalvo
      }
    });

  }

  onSubmit(event) {
    event.preventDefault();
    const { nome, email, login, senha } = this.state;

    const usuario = { id: this.props.match.params.id, nome: nome, email: email, login: login, senha: senha };
    this.put.editarUsuario(usuario)
      .then(response => {
        this.alert('sucesso');
      })
      .catch(error => {
        console.error(error)
        this.alert('erro');
      });
  }

  render() {

    return (
      <Layout>
        <CustomHeader />
        <Layout>
          <CustomSider />
          <Content className="content">
            {
              this.state.inputs.length > 0
                ? (
                  <React.Fragment>
                    <CustomForm
                      id={this.id}
                      titlePage="Tela de Edição de usuario"
                      inputs={this.state.inputs}
                      onSubmit={this.onSubmit.bind(this)}
                      onChange={this.onChange.bind(this)}
                    />
                    {
                      this.state.erro && (
                        <Alert message={MsgsEdicao.erro} type="error" showIcon />
                      )
                    }
                    {
                      this.state.sucesso && (
                        <Alert message={MsgsEdicao.sucesso} type="success" showIcon />
                      )
                    }
                  </React.Fragment>
                )
                : (
                  <h3 className="title-listagem"> Por favor tente mais tarde</h3>
                )
            }
          </Content>
        </Layout>
        <CustomFooter />
      </Layout>
    )
  }
}