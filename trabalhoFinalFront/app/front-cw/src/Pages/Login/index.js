import React, { Component } from "react";

import Authentication from "../../Api/Autentication";
import GetMethodApi from "../../Api/Get";
import Layout from "../../Middleware/Layout";
import Alert from '../../Middleware/Alert';
import CustomHeader from "../../Components/CustomHeader";
import CustomFooter from "../../Components/CustomFooter";
import CustomSider from "../../Components/CustomSider";
import CustomForm from "../../Components/CustomForm";
import Path from '../../Constants/Path';
import { MsgsLogin } from '../../Constants/Msg';
import inputModel from '../../Models/Input/InputsLogin'

const { Content } = Layout;

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      aguarde: false,
      erro: false,
      sucesso: false,
      usuarioLogado: localStorage.getItem('token')
    }
    this.api = new Authentication();
    this.get = new GetMethodApi();
    this.inputs = inputModel();
  }

  alert(type) {
    if (type === 'erro') {
      this.setState(state => { return { ...state, erro: true } })
      setTimeout(() => {
        this.setState(state => { return { ...state, erro: false } })
      }, 2000);
    }
    if(type === 'sucesso'){
      this.setState(state => { return { ...state, sucesso: true } })
      setTimeout(() => {
        this.setState(state => { return { ...state, sucesso: false } });
        window.location.href = Path.Home;
      }, 2000)
    }
  }

  onSubmit(event) {
    event.preventDefault();
    const { login, senha } = this.state;
    
    if( this.state.usuarioLogado ) return;

    this.api.logar({ login, senha })
      .then(response => {
        const { authorization } = response.headers;
        localStorage.setItem('token', authorization);
        this.setState(state => { return { ...state, usuarioLogado: localStorage.getItem('token') } });
        this.alert('sucesso')
      })
      .catch(error => {
        console.error(error);
        this.alert('erro');
      })
  }

  onChange(event){
    this.setState(state => {
      return {
        ...state,
        [event.target.name]: event.target.value
      }
    });
  }

  render() {
    return (
      <Layout>
        <CustomHeader />
        <Layout>
          <CustomSider />
          <Content className="content">
            {
              this.state.aguarde
                ? (<h1>Aguarde..</h1>)
                : (
                  <React.Fragment>
                    <CustomForm
                      titlePage="Tela de Login"
                      inputs={this.inputs}
                      onSubmit={this.onSubmit.bind(this)}
                      onChange={this.onChange.bind(this)}
                    />
                    {
                      this.state.erro && (
                        <Alert message={ MsgsLogin.erro } type="error" showIcon />
                      )
                    }
                    {
                      this.state.sucesso && (
                        <Alert message={ MsgsLogin.sucesso } type="success" showIcon />
                      )
                    }

                  </React.Fragment>
                )
            }
          </Content>
        </Layout>
        <CustomFooter />
      </Layout>
    )
  }
}
