import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Path from '../../Constants/Path';
import RotasPrivadas from './RotasPrivadas';

import Home from '../Home';
import Login from '../Login';

import Usuarios from '../ListaTodos/Usuarios';

import CadastroUsuario from '../Cadastro/Usuario';

import EdicaoUsuario from '../Edicao/Usuario';


export default class Rotas extends Component {
  render() {
    return (
      <Router>
        <Route path={ Path.Login } exact component={ Login } />
        <Route path={ Path.Cadastro.usuario } exact component={ CadastroUsuario } />
        
        <RotasPrivadas path={ Path.Home } exact component={ Home } />
        <RotasPrivadas path={ Path.Usuarios } exact component={ Usuarios } />
        <RotasPrivadas path={ Path.Edicao.usuario } exact component={ EdicaoUsuario } />
      </Router>
    )
  }
}