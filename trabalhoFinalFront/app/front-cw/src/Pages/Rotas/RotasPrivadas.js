import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Path from '../../Constants/Path';

const RotasPrivadas = ( { component: Component, ...resto } ) => (
  <Route { ...resto } render={ props => (
    localStorage.getItem( 'token' ) ?
    <Component { ...props } /> :
    <Redirect to={ { pathname: Path.Home, state: { from: props.location } } } />
  ) }
  />
);

export default RotasPrivadas;