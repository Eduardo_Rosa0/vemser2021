import React, { Component } from "react";

import Path from '../../../Constants/Path';
import Alert from '../../../Middleware/Alert';
import GetMethodApi from '../../../Api/Get';
import PostMethodApi from '../../../Api/Post';
import Layout from "../../../Middleware/Layout";
import CustomForm from "../../../Components/CustomForm";
import CustomHeader from "../../../Components/CustomHeader";
import CustomSider from "../../../Components/CustomSider";
import CustomFooter from "../../../Components/CustomFooter";
import { MsgsCadastro } from "../../../Constants/Msg";
import inputModel from '../../../Models/CadastroModels/InputsUsuario';


const { Content } = Layout;

export default class CadastroUsuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      erro: false,
      sucesso: false,
    };
    this.get = new GetMethodApi();
    this.post = new PostMethodApi();
    this.inputs = inputModel();
    // [
    //   {
    //     textoLabel: '* Nome',
    //     name: 'nome',
    //     id: 'nome',
    //     type: 'text',
    //     placeholder: '[ Nome completo ]'
    //   },
    //   {
    //     textoLabel: '* E-mail',
    //     name: 'email',
    //     id: 'email',
    //     type: 'email',
    //     placeholder: '[ **@**.com ]'
    //   },
    //   {
    //     textoLabel: '* Login',
    //     name: 'login',
    //     id: 'login',
    //     type: 'text',
    //     placeholder: '[ Login do usuário ]'
    //   },
    //   {
    //     textoLabel: '* Senha',
    //     name: 'senha',
    //     id: 'senha',
    //     type: 'password',
    //     placeholder: '[ ***** ]'
    //   },
    // ];
  }

  alert(type) {
    if (type === 'erro') {
      this.setState(state => { return { ...state, erro: true } })
      setTimeout(() => {
        this.setState(state => { return { ...state, erro: false } })
      }, 2000);
    }
    if (type === 'sucesso') {
      this.setState(state => { return { ...state, sucesso: true } })
      setTimeout(() => {
        this.setState(state => { return { ...state, sucesso: false } });
        
        window.location.href = localStorage.getItem('token') ? Path.Cadastro.usuario : Path.Login;
      }, 2000)
    }
  }

  onSubmit(event) {
    event.preventDefault();
    const { nome, email, login, senha } = this.state;
    const usuario = { nome: nome, email: email, login: login, senha: senha };

    this.post.salvarUsuario(usuario)
      .then(response => {
        console.log(`Usuario salvo`);
        console.log(usuario);
        this.alert('sucesso')
      })
      .catch(error => {
        console.error(error);
        this.alert('erro');
      });
  }

  onChange(event) {
    this.setState(state => {
      return {
        ...state,
        [event.target.name]: event.target.value
      }
    });
  }

  render() {
    return (
      <Layout>
        <CustomHeader />
        <Layout>
          <CustomSider />
          <Content className="content">
            <React.Fragment>
              <CustomForm
                titlePage="Tela de cadastro de usuario"
                inputs={this.inputs}
                onSubmit={this.onSubmit.bind(this)}
                onChange={this.onChange.bind(this)}
              />
              {
                this.state.erro && (
                  <Alert message={MsgsCadastro.erro} type="error" showIcon />
                )
              }
              {
                this.state.sucesso && (
                  <Alert message={MsgsCadastro.sucesso} type="success" showIcon />
                )
              }
            </React.Fragment>
          </Content>
        </Layout>
        <CustomFooter />
      </Layout>
    )
  }
}