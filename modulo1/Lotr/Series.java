import java.util.*;

public class Series {

    public void imprimirSeries() {
        ArrayList<String> series = new ArrayList<>();
        
        series.add("Greys Anatomy");
        series.add("Vikings");
        series.add("The good place");
        series.add("Breaking bad");
        series.add("Supernatural");
        series.add("Mr Robot");
        series.add("Happy");
        series.add("Prison break");
        series.add("Ozark");
        series.add("To your Eternity");
        series.add("Game of thrones");
        series.add("Haikyuu");
        series.add("The Witcher");
        series.add("The office");
        series.add("Naruto");
        series.add("Peaky blinders");
        
        /*System.out.println( series.get(7) );
        series.isEmpty();
        series.remove(9);
        series.remove(new Object("Peaky blinders"));*/
        
        for( String serie : series ){
            System.out.println( serie );
        }
        
    }
    
}