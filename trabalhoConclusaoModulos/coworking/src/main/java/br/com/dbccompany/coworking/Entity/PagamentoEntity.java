package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table(name = "PAGAMENTO")
public class PagamentoEntity {
    @Id
    @SequenceGenerator( name = "PGTO_SEQ", sequenceName = "PGTO_SEQ" )
    @GeneratedValue( generator = "PGTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private TipoPagamentoEnum tipoPagamento;

    @ManyToOne
    @JoinColumn(name = "contratacao")
    private ContratacaoEntity contratacao;

    @ManyToOne
    @JoinColumn(name = "pacote")
    private ClientePacoteEntity pacote;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public ClientePacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(ClientePacoteEntity pacote) {
        this.pacote = pacote;
    }
}
