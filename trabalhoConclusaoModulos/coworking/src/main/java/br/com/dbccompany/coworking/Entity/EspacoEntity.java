package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ESPACOS")
public class EspacoEntity {

    @Id
    @SequenceGenerator( name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ" )
    @GeneratedValue( generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(unique = true)
    private String nome;

    private Integer quantidadePessoas;

    private Double valor;

    @OneToMany( mappedBy = "espaco")
    private List<EspacoPacoteEntity> pacotes;

    @OneToMany( mappedBy = "espaco")
    private List<ContratacaoEntity> contratacoes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(Integer quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacoPacoteEntity> getPacotes() {
        return pacotes;
    }

    public void setPacotes(List<EspacoPacoteEntity> pacotes) {
        this.pacotes = pacotes;
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
