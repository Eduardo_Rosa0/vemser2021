package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ClienteDTO {

    private Integer id;
    private String nome;
    private String cpf;
    private LocalDate dataNascimento;
    private List<Integer> contatos;
    private List<Integer> pacotes;
    private List<Integer> contratacoes;

    public ClienteDTO(ClienteEntity cliente) {
        List<Integer> ctts = parseListContatos(cliente.getContatos());
        List<Integer> pct = parseListPacotes(cliente.getPacotes());
        List<Integer> crts = parseListContratacoes(cliente.getContratacoes());

        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.cpf = this.conveterParaString(cliente.getCpf());
        this.dataNascimento = cliente.getDataNascimento();
        this.contatos = ctts;
        this.pacotes = pct;
        this.contratacoes = crts;
    }

    public ClienteDTO(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<Integer> getContatos() {
        return contatos;
    }

    public List<Integer> getPacotes() {
        return pacotes;
    }

    public void setPacotes(List<Integer> pacotes) {
        this.pacotes = pacotes;
    }

    public List<Integer> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Integer> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public void setContatos(List<Integer> contatos) {
        this.contatos = contatos;
    }

    public Character [] converteParaChar(String valor){
        Character [] array = new Character[11];
        for(int i = 0; i < valor.length(); i++){
            array[i] = valor.charAt(i);
        }
        return array;
    }

    public String conveterParaString(Character [] array){
        String cpf = "";
        for (int i = 0; i < array.length; i++) {
            cpf += array[i];

        }
        return cpf;
    }

    public List<Integer> parseListContatos (List<ContatoEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids = new ArrayList<>();
        for( ContatoEntity ctt : list ){
            ids.add( ctt.getId() );
        }

        return ids;
    }

    public List<Integer> parseListPacotes (List<ClientePacoteEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids = new ArrayList<>();
        for( ClientePacoteEntity ctt : list ){
            ids.add( ctt.getId() );
        }

        return ids;
    }

    public List<Integer> parseListContratacoes (List<ContratacaoEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids = new ArrayList<>();
        for( ContratacaoEntity ctt : list ){
            ids.add( ctt.getId() );
        }

        return ids;
    }

}
