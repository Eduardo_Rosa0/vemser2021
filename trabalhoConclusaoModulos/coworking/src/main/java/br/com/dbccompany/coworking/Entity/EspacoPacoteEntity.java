package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table(name = "ESPACO_PACOTE")
public class EspacoPacoteEntity {

    @Id
    @SequenceGenerator( name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ" )
    @GeneratedValue( generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "espaco")
    private EspacoEntity espaco;

    @ManyToOne
    @JoinColumn(name = "pacote")
    private PacoteEntity pacote;

    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}

