package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Autowired
    private ClienteRepository repositoryCliente;

    @Autowired
    private TipoContatoRepository repositoryTipo;

    @Transactional (rollbackOn = Exception.class)
    public ContatoDTO salvar(ContatoEntity contato) throws Exception {
        try{

            if( contato.getTipo() != null ) {
                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                return new ContatoDTO(repository.save(contato));
            }
            throw new ExceptionGeneric( "Verifique os dados" );
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public ContatoDTO editar(ContatoEntity contato, Integer id) throws Exception {
        try{
            Optional<ContatoEntity> contatoEntity = repository.findById(id);
            if(contatoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            contato.setId(id);
            return salvar(contato);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public ContatoDTO findById(Integer id) throws Exception {
        try{
            Optional<ContatoEntity> contatoEntity = repository.findById(id);
            if(contatoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );
             return new ContatoDTO(contatoEntity.get());
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ContatoDTO> findAllByTipo(TipoContatoEntity tipo) throws Exception {
        try{
            List<ContatoDTO> contatoDTOList =  parseList(repository.findAllByTipo(tipo));
            if(contatoDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return contatoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }


    public List<ContatoDTO> findAllByCliente(ClienteEntity cliente) throws Exception {
        try{
            List<ContatoDTO> contatoDTOList =  parseList(repository.findAllByCliente(cliente));
            if(contatoDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return contatoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ContatoDTO> findAll() throws Exception {
        try{
            List<ContatoDTO> contatoDTOList = parseList(repository.findAll());
            if(contatoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return contatoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }


    public List<ContatoDTO> findAllByDescricao( String descricao) throws Exception {
        try{
            List<ContatoDTO> contatoDTOList = parseList(repository.findAllByDescricao(descricao));
            if(contatoDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return contatoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ContatoDTO> parseList(List<ContatoEntity> list){
        if(list == null) return null;
        List<ContatoDTO> contatoDTOList = new ArrayList<>();

        for(ContatoEntity contatoEntity : list){
            contatoDTOList.add( new ContatoDTO(contatoEntity) );
        }

        if(contatoDTOList.size() > 0)
            return contatoDTOList;
        return null;
    }

    public ContatoEntity parseEntity(ContatoDTO dto){
        ContatoEntity ctt = new ContatoEntity();
        TipoContatoEntity tipo = getTipo(dto.getTipoContatoId());
        ClienteEntity cli = getCliente(dto.getClienteId());

        ctt.setId(dto.getId());
        ctt.setDescricao(dto.getDescricao());
        ctt.setCliente(cli);
        ctt.setTipo(tipo);

        return ctt;
    }

    public TipoContatoEntity getTipo(Integer id){
        if(id == null) return null;

        Optional<TipoContatoEntity> tipo = repositoryTipo.findById(id);
        return tipo.isPresent() ? tipo.get() : null;
    }

    public ClienteEntity getCliente(Integer id){
        if(id == null) return null;

        Optional<ClienteEntity> cli =  repositoryCliente.findById(id);
        return cli.isPresent() ? cli.get() : null;
    }
}
