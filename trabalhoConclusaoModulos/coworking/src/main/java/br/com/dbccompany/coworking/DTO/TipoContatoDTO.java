package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

import java.util.ArrayList;
import java.util.List;

public class TipoContatoDTO {
    private Integer id;
    private String nome;
    private List<Integer> contatos;

    public TipoContatoDTO(TipoContatoEntity tipoContato){
        List<Integer> list = parseListContatos(tipoContato.getContatos());

        this.id = tipoContato.getId();
        this.nome = tipoContato.getNome();

        if(list != null && list.size() > 0 )
            this.contatos = parseListContatos(tipoContato.getContatos());
    }
    public TipoContatoDTO(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Integer> getContatos() {
        return contatos;
    }

    public void setContatos(List<Integer> contatos) {
        this.contatos = contatos;
    }

    public List<Integer> parseListContatos (List<ContatoEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids = new ArrayList<>();
        for( ContatoEntity ctt : list ){
            ids.add( ctt.getId() );
        }

        return ids;
    }

}
