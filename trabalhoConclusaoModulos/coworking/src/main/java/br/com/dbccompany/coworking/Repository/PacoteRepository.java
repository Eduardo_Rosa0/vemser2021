package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PacoteRepository extends CrudRepository<PacoteEntity,Integer> {
    @Override
    Optional<PacoteEntity> findById( Integer integer );

    List<PacoteEntity> findAll();

    List<PacoteEntity> findAllByValor(Double valor );
}
