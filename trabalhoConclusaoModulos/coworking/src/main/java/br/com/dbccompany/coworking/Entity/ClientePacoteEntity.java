package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity(name = "CLIENTE_PACOTE")
public class ClientePacoteEntity {

    @Id
    @SequenceGenerator( name = "CLI_PACOTE_SEQ", sequenceName = "CLI_PACOTE_SEQ" )
    @GeneratedValue( generator = "CLI_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "cliente")
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn(name = "pacote")
    private PacoteEntity pacote;

    private Integer quantidade;

    @OneToMany(mappedBy = "pacote")
    private List<PagamentoEntity> pagamentos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
