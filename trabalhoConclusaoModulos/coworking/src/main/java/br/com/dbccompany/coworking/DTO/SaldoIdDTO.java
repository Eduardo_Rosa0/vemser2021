package br.com.dbccompany.coworking.DTO;

public class SaldoIdDTO {
    private Integer clienteId;
    private Integer espacoId;

    public SaldoIdDTO() {
    }

    public SaldoIdDTO(Integer clienteId, Integer espacoId) {
        this.clienteId = clienteId;
        this.espacoId = espacoId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer espacoId) {
        this.espacoId = espacoId;
    }
}
