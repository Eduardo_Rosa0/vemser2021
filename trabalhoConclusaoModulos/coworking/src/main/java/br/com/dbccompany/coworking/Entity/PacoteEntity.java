package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PACOTES")
public class PacoteEntity {
    @Id
    @SequenceGenerator( name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ" )
    @GeneratedValue( generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private Double valor;

    @OneToMany( mappedBy = "pacote")
    private List<ClientePacoteEntity> clientes;

    @OneToMany( mappedBy = "pacote")
    private List<EspacoPacoteEntity> espacos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<ClientePacoteEntity> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClientePacoteEntity> clientes) {
        this.clientes = clientes;
    }

    public List<EspacoPacoteEntity> getEspacos() {
        return espacos;
    }

    public void setEspacos(List<EspacoPacoteEntity> espacos) {
        this.espacos = espacos;
    }
}
