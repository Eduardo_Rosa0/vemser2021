package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.SaldoDTO;
import br.com.dbccompany.coworking.DTO.SaldoIdDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping( value = "/api/cw/saldo" )
public class SaldoClienteController {
    @Autowired
    private SaldoClienteService service;


    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarTipoContato(@RequestBody SaldoDTO saldoDTO){
        try{
            return new ResponseEntity<>( service.save( service.parseEntity(saldoDTO) ), HttpStatus.CREATED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/id")
    @ResponseBody
    public ResponseEntity<Object> findById( @RequestBody SaldoIdDTO id ){
        try{
            return new ResponseEntity<>(service.findById( service.parseId(id.getClienteId(), id.getEspacoId()) ), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<Object>  findAll(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/all/cliente/{idCliente}")
    @ResponseBody
    public ResponseEntity<Object>  findAllByCliente( @PathVariable Integer idCliente ){
        try{
            return new ResponseEntity<>(service.findAllByCliente(idCliente), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/all/espaco/{idEspaco}")
    @ResponseBody
    public ResponseEntity<Object> findAllByEspaco( @PathVariable Integer idEspaco ){
        try{
            return new ResponseEntity<>(service.findAllByEspaco(idEspaco), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
