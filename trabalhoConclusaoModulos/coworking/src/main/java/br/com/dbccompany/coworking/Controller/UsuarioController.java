package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(value = "/api/cw/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvar (@RequestBody UsuarioDTO usuario){
        try{
            return new ResponseEntity<>( service.salvar(usuario.converter()) , HttpStatus.OK);
        } catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarTipoContato(@RequestBody UsuarioDTO usuarioDTO, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar( usuarioDTO.converter() , id), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<Object> findAll(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> buscarPorId( @PathVariable Integer id ){
        try{
            return new ResponseEntity<>(service.findById( id ), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}