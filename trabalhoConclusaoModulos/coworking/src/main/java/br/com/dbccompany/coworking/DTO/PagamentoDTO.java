package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;

public class PagamentoDTO {

    private Integer id;
    private Integer contratacaoId;
    private Integer clientePacoteId;
    private TipoPagamentoEnum tipoPagamento;

    public PagamentoDTO(){}

    public PagamentoDTO( PagamentoEntity pagamento ) {
        this.id = pagamento.getId();
        this.contratacaoId = pagamento.getContratacao().getId();
        this.tipoPagamento = pagamento.getTipoPagamento();
        this.clientePacoteId = pagamento.getPacote().getId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContratacaoId() {
        return contratacaoId;
    }

    public void setContratacaoId(Integer contratacaoId) {
        this.contratacaoId = contratacaoId;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public Integer getClientePacoteId() {
        return clientePacoteId;
    }

    public void setClientePacoteId(Integer clientePacoteId) {
        this.clientePacoteId = clientePacoteId;
    }
}
