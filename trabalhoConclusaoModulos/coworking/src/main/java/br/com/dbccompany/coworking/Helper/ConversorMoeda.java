package br.com.dbccompany.coworking.Helper;

public class ConversorMoeda {
    public static String doubleParaString(Double valor){
        StringBuilder novoValor = new StringBuilder();
        novoValor.append("R$ ");
        novoValor.append(valor);
        return novoValor.toString().replace(".", ",");
    }

    public static Double stringParaDouble(String valor) {
        if(valor.equals("")) return null;

        if( ! (valor.matches("^(?i)r\\$(.*)\\s(.*),(.*)[0-9]{2}")) ) return null;

        return Double.parseDouble(valor.replace("R$", "")
                .replace(".", "")
                .replace(" ", "")
                .replace(",", "."));
    }
}
