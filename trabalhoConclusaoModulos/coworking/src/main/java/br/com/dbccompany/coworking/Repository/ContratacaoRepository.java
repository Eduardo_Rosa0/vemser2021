package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity,Integer> {
    @Override
    Optional<ContratacaoEntity> findById(Integer integer);

    List<ContratacaoEntity> findAllByEspaco( EspacoEntity espaco );

    List<ContratacaoEntity> findAll();

    List<ContratacaoEntity> findAllByCliente( ClienteEntity cliente );

    List<ContratacaoEntity> findAllByTipoContratacao( String tipoContratacao );

    List<ContratacaoEntity> findAllByPrazo( Integer prazo );
}
