package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity,Integer> {
    @Override
    Optional<PagamentoEntity> findById( Integer integer );

    List<PagamentoEntity> findAll();

    List<PagamentoEntity> findAllByTipoPagamento( String tipoPagamento );

    List<PagamentoEntity> findAllByContratacao( ContratacaoEntity contratacao );

    List<PagamentoEntity> findAllByPacote( ClientePacoteEntity pacote );
}
