package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessoEntity;

import java.time.LocalDateTime;

public class AcessoDTO {
    private Integer id;
    private Integer clienteId;
    private Integer espacoId;
    private Boolean entrada;
    private LocalDateTime data;
    private Boolean excessao = false;
    private String mensagem;

    public AcessoDTO(AcessoEntity acesso) {
        this.id = acesso.getId();
        this.clienteId = acesso.getSaldoCliente().getId().getId_cliente();
        this.espacoId = acesso.getSaldoCliente().getId().getId_espaco();
        this.entrada = acesso.getEntrada();
        this.data = acesso.getData();
        this.excessao = acesso.getExcecao();
    }

    public AcessoDTO(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer espacoId) {
        this.espacoId = espacoId;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getExcessao() {
        return excessao;
    }

    public void setExcessao(Boolean excessao) {
        this.excessao = excessao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
