package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository repository;

    @Autowired
    private ContratacaoRepository repositoryContratacao;

    @Autowired
    private ClientePacoteRepository repositoryClientePacote;

    @Transactional(rollbackOn = Exception.class)
    public PagamentoDTO save(PagamentoEntity pagamento) throws Exception {
        try{
            if( pagamento.getPacote() == null || pagamento.getContratacao() == null ) {
                if (pagamento.getContratacao() == null) {
                    pagamento.setPacote(pagamento.getPacote());
                }
                 pagamento.setContratacao(pagamento.getContratacao());

                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                return new PagamentoDTO(repository.save(pagamento));
            }
            throw new ExceptionGeneric( "Verifique os dados" );

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public PagamentoDTO update( PagamentoEntity pagamento, Integer id ) throws Exception {
        try{
            Optional<PagamentoEntity> pagamentoEntity = repository.findById(id);
            if(pagamentoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            pagamento.setId(id);
            return save(pagamento);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public PagamentoDTO findById(Integer id ) throws Exception {
        try{
            Optional<PagamentoEntity> pagamentoEntity = repository.findById(id);
            if(pagamentoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            return new PagamentoDTO(pagamentoEntity.get());
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PagamentoDTO> findAll() throws Exception {
        try{
            List<PagamentoDTO> pagamentoDTOList =  parseList( repository.findAll() );
            if(pagamentoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return pagamentoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PagamentoDTO> findAllByTipoPagamento( String tipo ) throws Exception {
        try{
            List<PagamentoDTO> pagamentoDTOList =  parseList( repository.findAllByTipoPagamento(tipo) );
            if(pagamentoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return pagamentoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PagamentoDTO> findAllByContratacao( ContratacaoEntity contratacao ) throws Exception {
        try{
            List<PagamentoDTO> pagamentoDTOList = parseList( repository.findAllByContratacao(contratacao) );
            if(pagamentoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return pagamentoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PagamentoDTO> findAllByPacote( ClientePacoteEntity clientePacote ) throws Exception {
        try{
            List<PagamentoDTO> pagamentoDTOList = parseList( repository.findAllByPacote(clientePacote) );
            if(pagamentoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return pagamentoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PagamentoDTO> parseList(List<PagamentoEntity> list){
        if(list == null || list.size() == 0 ) return null;

        List<PagamentoDTO> dtos = new ArrayList<>();
        for(PagamentoEntity pagto : list){
            dtos.add(new PagamentoDTO( pagto ));
        }
        return dtos;
    }

    public PagamentoEntity parseEntity( PagamentoDTO dto ){
        PagamentoEntity pagamentoEntity = new PagamentoEntity();

        pagamentoEntity.setId(dto.getId());
        pagamentoEntity.setTipoPagamento(dto.getTipoPagamento());
        pagamentoEntity.setContratacao( buscaContratacao(dto.getContratacaoId()) );
        pagamentoEntity.setPacote( buscaClientePacote(dto.getClientePacoteId()) );

        return pagamentoEntity;
    }

    public ClientePacoteEntity buscaClientePacote( Integer id ){
        if(id == null) return null;
        Optional<ClientePacoteEntity> clientePacoteEntity = repositoryClientePacote.findById(id);

        if(clientePacoteEntity.isEmpty())
            return null;

        return clientePacoteEntity.get();

    }

    public ContratacaoEntity buscaContratacao( Integer id ){
        if(id == null) return null;
        Optional<ContratacaoEntity> contratacaoEntity = repositoryContratacao.findById(id);

        if(contratacaoEntity.isEmpty())
            return null;

        return contratacaoEntity.get();

    }

}
