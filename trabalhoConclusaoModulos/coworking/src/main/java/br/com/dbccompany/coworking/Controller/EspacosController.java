package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping( value = "/api/cw/espaco" )
public class EspacosController {

    @Autowired
    private EspacoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarEspaco(@RequestBody EspacoDTO dto){
        try{
            return new ResponseEntity<>( service.salvar( service.parseEntity(dto) ), HttpStatus.CREATED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarEspaco(@RequestBody EspacoDTO tipo, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar( service.parseEntity(tipo) , id), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<Object>  buscarTodos(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping (value = "/pagamentos")
    @ResponseBody
    public ResponseEntity<Object> buscarTodosPagamentos(@PathVariable Integer espacoid){
        try{
            return new ResponseEntity<>( service.buscarTodosPagamentos(espacoid), HttpStatus.CREATED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping (value = "/pacotes")
    @ResponseBody
    public ResponseEntity<Object> buscarTodosPacotes(@PathVariable Integer espacoid){
        try{
            return new ResponseEntity<>( service.buscarTodosPacotes(espacoid), HttpStatus.CREATED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
