package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;
    @Autowired
    private ClienteRepository repositoryCliente;
    @Autowired
    private EspacoRepository repositoryEspaco;
    @Autowired
    private PagamentoRepository repositoryPagamento;

    @Transactional(rollbackOn = Exception.class)
    public ContratacaoDTO salvar(ContratacaoEntity contratacao) throws Exception {
        try{
            EspacoEntity espaco = contratacao.getEspaco();
            ClienteEntity cliente = contratacao.getCliente();

            if( espaco != null && cliente != null ){
                ContratacaoEntity contratacaoEntity = repository.save(contratacao);

                List<ContratacaoEntity> contratacaoEntityList = espaco.getContratacoes();
                contratacaoEntityList.add(contratacaoEntity);
                espaco.setContratacoes(contratacaoEntityList);
                repositoryEspaco.save(espaco);

                List<ContratacaoEntity> contratacaoEntityList1 = cliente.getContratacoes();
                contratacaoEntityList1.add( contratacaoEntity );
                cliente.setContratacoes( contratacaoEntityList1 );
                repositoryCliente.save(cliente);

                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                return new ContratacaoDTO(contratacao);
            }
            throw new ExceptionGeneric( "Verifique os dados" );
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public ContratacaoDTO editar(ContratacaoEntity contratacao, Integer id) throws Exception {
        try{
            Optional<ContratacaoEntity> contratacaoEntity = repository.findById(id);
            if(contratacaoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            contratacao.setId(id);
            return salvar(contratacao);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public ContratacaoDTO findById(Integer id) throws Exception {
        try{
            Optional<ContratacaoEntity> contratacaoEntity = repository.findById(id);
            if(contratacaoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            return new ContratacaoDTO(contratacaoEntity.get());
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ContratacaoDTO> findAll() throws Exception {
        try{
            List<ContratacaoDTO> contratacaoDTOList =  parseList(repository.findAll());
            if(contratacaoDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return contratacaoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ContratacaoDTO> findAllByCliente( Integer id ) throws Exception {
        try{
            Optional<ClienteEntity> clienteEntity = repositoryCliente.findById(id);
            if(clienteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            List<ContratacaoDTO> contratacaoDTOList = parseList(repository.findAllByCliente( clienteEntity.get() ));
            if(contratacaoDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return contratacaoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }


    public List<ContratacaoDTO> findAllByEspaco( Integer id ) throws Exception {
        try{

            Optional<EspacoEntity> espacoEntity = repositoryEspaco.findById(id);
            if(espacoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            List<ContratacaoDTO> contratacaoDTOList = parseList(repository.findAllByEspaco( espacoEntity.get() ));
            if(contratacaoDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return contratacaoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ContratacaoDTO> parseList(List<ContratacaoEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<ContratacaoDTO> dtos = new ArrayList<>();

        for( ContratacaoEntity contratacao : list ){
            dtos.add( new ContratacaoDTO(contratacao) );
        }
        return dtos;
    }

    public ContratacaoEntity parseEntity( ContratacaoDTO dto ){
        ContratacaoEntity contratacaoEntity = new ContratacaoEntity();

        contratacaoEntity.setId(dto.getId());
        contratacaoEntity.setQuantidade(dto.getQuantidade());
        contratacaoEntity.setQuantidade(dto.getQuantidade());
        contratacaoEntity.setDesconto(dto.getDesconto());
        contratacaoEntity.setEspaco( buscaEspaco(dto.getEspacoId()) );
        contratacaoEntity.setCliente( buscaCliente(dto.getClienteId()) );
        contratacaoEntity.setPagamentos( buscaPagamentos(dto.getPagamentosId()) );
        contratacaoEntity.setTipoContratacao(dto.getTipoContratacao());

        return contratacaoEntity;
    }

    public ClienteEntity buscaCliente( Integer id ){
        if(id == null) return null;
        Optional<ClienteEntity> clienteEntity =  repositoryCliente.findById(id);
        if(clienteEntity.isEmpty())
            return null;

        return clienteEntity.get();
    }

    public EspacoEntity buscaEspaco( Integer id ){
        if(id == null) return null;
        Optional<EspacoEntity> clienteEntity =  repositoryEspaco.findById(id);
        if(clienteEntity.isEmpty())
            return null;

        return clienteEntity.get();
    }

    public List<PagamentoEntity> buscaPagamentos( List<Integer> ids){
        if(ids == null) return null;
        List<PagamentoEntity> pagamentoEntityList = new ArrayList<>();
        for( Integer id : ids ){
            Optional<PagamentoEntity> pagamentoEntity = repositoryPagamento.findById(id);
            if(pagamentoEntity.isPresent())
                pagamentoEntityList.add(pagamentoEntity.get());
        }
        return pagamentoEntityList;
    }

}
