package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacoPacoteService {

    @Autowired
    private EspacoPacoteRepository repository;
    @Autowired
    private EspacoRepository repositoryEspaco;
    @Autowired
    private PacoteRepository repositoryPacote;

    @Transactional(rollbackOn = Exception.class)
    public EspacoPacoteDTO save(EspacoPacoteEntity espacoPacote ) throws Exception {
        try{
            PacoteEntity pacote = espacoPacote.getPacote();
            EspacoEntity espaco = espacoPacote.getEspaco();

            if( espaco != null && pacote != null ){
                EspacoPacoteEntity espacoPacoteEntity = repository.save(espacoPacote);

                List<EspacoPacoteEntity> espacoPacoteEntityList = espaco.getPacotes();
                espacoPacoteEntityList.add( espacoPacoteEntity );
                espaco.setPacotes( espacoPacoteEntityList );

                List<EspacoPacoteEntity> espacoPacoteEntityList1 = pacote.getEspacos();
                espacoPacoteEntityList1.add( espacoPacoteEntity );
                pacote.setEspacos( espacoPacoteEntityList1 );

                repositoryEspaco.save( espaco );
                repositoryPacote.save( pacote );

                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                return new EspacoPacoteDTO( espacoPacoteEntity );
            }
            throw new ExceptionGeneric( "Verifique os dados" );

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public EspacoPacoteDTO update( EspacoPacoteEntity espacoPacote, Integer id ) throws Exception {
        try{
            Optional<EspacoPacoteEntity> espacoPacoteEntity = repository.findById(id);
            if (espacoPacoteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            espacoPacote.setId(id);
            return new EspacoPacoteDTO(repository.save(espacoPacote));
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public EspacoPacoteDTO findById( Integer id ) throws Exception {
        try{
            Optional<EspacoPacoteEntity> espacoPacoteEntity = repository.findById(id);
            if (espacoPacoteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            return new EspacoPacoteDTO(espacoPacoteEntity.get());
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoPacoteDTO> findAll() throws Exception {
        try{
            List<EspacoPacoteDTO>  espacoPacoteDTOList = parseList( repository.findAll() );

            if(espacoPacoteDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return espacoPacoteDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoPacoteDTO> findAllByPacote(PacoteEntity pacote) throws Exception {
        try{
            List<EspacoPacoteDTO>  espacoPacoteDTOList = parseList( repository.findAllByPacote(pacote) );

            if(espacoPacoteDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return espacoPacoteDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoPacoteDTO> findAllByTipoContratacao( String tipo ) throws Exception {
        try {
            List<EspacoPacoteDTO>  espacoPacoteDTOList = parseList( repository.findAllByTipoContratacao(tipo) );

            if(espacoPacoteDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return espacoPacoteDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoPacoteDTO> findAllByPrazo( Integer prazo ) throws Exception {
        try {
            List<EspacoPacoteDTO>  espacoPacoteDTOList = parseList( repository.findAllByPrazo( prazo ) );

            if(espacoPacoteDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return espacoPacoteDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoPacoteDTO> findAllByEspaco(EspacoEntity espaco) throws Exception {
        try{
            List<EspacoPacoteDTO>  espacoPacoteDTOList = parseList( repository.findAllByEspaco(espaco) );

            if(espacoPacoteDTOList.size() == 0)
                throw new ExceptionGeneric( "Verifique os dados" );

            return espacoPacoteDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoPacoteDTO> parseList(List<EspacoPacoteEntity> list){
        if(list == null || list.size() == 0 ) return null;

        List<EspacoPacoteDTO> dtos = new ArrayList<>();
        for(EspacoPacoteEntity espacoPacote : list){
            dtos.add(new EspacoPacoteDTO(espacoPacote));
        }
        return dtos;
    }

    public EspacoPacoteEntity parseEntity(EspacoPacoteDTO dto ){
        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();

        espacoPacote.setId( dto.getId() );
        espacoPacote.setQuantidade( dto.getQuantidade() );
        espacoPacote.setPrazo( dto.getPrazo() );
        espacoPacote.setTipoContratacao( dto.getTipoContratacao() );
        espacoPacote.setEspaco(getEspaco( dto.getEspacoId() ));
        espacoPacote.setPacote(getPacote( dto.getPacoteId() ));

        return espacoPacote;
    }

    public EspacoEntity getEspaco(Integer id){
        Optional<EspacoEntity> espaco = repositoryEspaco.findById(id);
        return espaco != null ? espaco.get() : null;
    }

    public PacoteEntity getPacote(Integer id){
        Optional<PacoteEntity> pacote = repositoryPacote.findById(id);
        return pacote != null ? pacote.get() : null;
    }
}
