package br.com.dbccompany.coworking.DTO;


import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Helper.ConversorMoeda;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PacoteDTO {
    private Integer id;
    private String valor;
    private List<Integer> espacoPacoteIds;
    private List<Integer> clientePacoteIds;


    public PacoteDTO(PacoteEntity pacote){
        this.id = pacote.getId();
        this.valor = ConversorMoeda.doubleParaString(pacote.getValor());
        this.clientePacoteIds = parseListClientePacote(pacote.getClientes());
        this.espacoPacoteIds = parseListEspacoPacote(pacote.getEspacos());
    }

    public PacoteDTO(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<Integer> getEspacoPacoteIds() {
        return espacoPacoteIds;
    }

    public void setEspacoPacoteIds(List<Integer> espacoPacoteIds) {
        this.espacoPacoteIds = espacoPacoteIds;
    }

    public List<Integer> getClientePacoteIds() {
        return clientePacoteIds;
    }

    public void setClientePacoteIds(List<Integer> cliente) {
        this.clientePacoteIds = cliente;
    }

    public String converterEmString(Double valor){
        Locale padraoBR = new Locale("pt", "BR");
        return NumberFormat.getCurrencyInstance().format(valor);
    }

    public Double converterEmDouble(String valor){
        return Double.parseDouble(valor.replace("R$","")
            .replace(" ","")
            .replace(".","")
            .replace(",",""));
    }

    public List<Integer> parseListClientePacote(List<ClientePacoteEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids= new ArrayList<>();
        for( ClientePacoteEntity cli : list ){
            ids.add(cli.getId());
        }

        return ids;
    }

    public List<Integer> parseListEspacoPacote(List<EspacoPacoteEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids= new ArrayList<>();
        for( EspacoPacoteEntity espacoPacoteEntity : list ){
            ids.add(espacoPacoteEntity.getId());
        }

        return ids;
    }

}
