package br.com.dbccompany.coworking.Entity;

public enum TipoContratacaoEnum {
    MINUTO, HORA, TURNOS, DIARIA, SEMANA, MES
}
