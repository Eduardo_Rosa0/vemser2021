package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

public class EspacoPacoteDTO {
    private Integer id;
    private Integer espacoId;
    private Integer pacoteId;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    public EspacoPacoteDTO( EspacoPacoteEntity espacoPacote){
        this.id = espacoPacote.getId();
        this.espacoId = espacoPacote.getEspaco().getId();
        this.pacoteId = espacoPacote.getPacote().getId();
        this.tipoContratacao = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
    }

    public EspacoPacoteDTO(){

    }

    public Integer getPacoteId() {
        return pacoteId;
    }

    public void setPacoteId(Integer pacoteId) {
        this.pacoteId = pacoteId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer espacoId) {
        this.espacoId = espacoId;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
