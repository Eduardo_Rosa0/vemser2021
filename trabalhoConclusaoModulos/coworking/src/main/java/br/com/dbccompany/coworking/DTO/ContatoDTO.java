package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;

public class ContatoDTO {
    private Integer id;
    private Integer tipoContatoId;
    private Integer clienteId;
    private String descricao;

    public ContatoDTO(ContatoEntity contato){
        this.id = contato.getId();
        this.descricao = contato.getDescricao();
        if(contato.getTipo() != null)
            this.tipoContatoId = contato.getTipo().getId();
        if(contato.getCliente() != null)
            this.clienteId = contato.getCliente().getId();
    }

    public ContatoDTO() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTipoContatoId() {
        return tipoContatoId;
    }

    public void setTipoContatoId(Integer tipoContatoId) {
        this.tipoContatoId = tipoContatoId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
