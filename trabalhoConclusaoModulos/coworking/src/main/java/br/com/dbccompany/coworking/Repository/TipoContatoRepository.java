package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity,Integer> {
    @Override
    Optional<TipoContatoEntity> findById( Integer integer );

    List<TipoContatoEntity> findAll();

    TipoContatoEntity findByNome(String nome );
}
