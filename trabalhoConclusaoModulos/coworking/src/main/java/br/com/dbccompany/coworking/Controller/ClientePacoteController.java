package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping( value = "/api/cw/clientePacote" )
public class ClientePacoteController {
    @Autowired
    private ClientePacoteService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarContato(@RequestBody ClientePacoteDTO clientePacoteDTO){
        try{
            return new ResponseEntity<>( service.save(service.parseEntity(clientePacoteDTO)) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarContato(@RequestBody ClientePacoteDTO clientePacoteDTO, @PathVariable Integer id){
        try{
            return new ResponseEntity<>( service.update( service.parseEntity(clientePacoteDTO), id) ,HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<Object> findAll(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping (value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.findById(id), HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/all/cliente/{idCliente}")
    @ResponseBody
    public ResponseEntity<Object> findAllByCliente(@PathVariable Integer idCliente){
        try{
            return new ResponseEntity<>( service.findAllByCliente(idCliente) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/all/pacote/{idPacote}")
    @ResponseBody
    public ResponseEntity<Object> findAllByPacote(@PathVariable Integer idPacote){
        try{
            return new ResponseEntity<>( service.findAllByCliente(idPacote) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
