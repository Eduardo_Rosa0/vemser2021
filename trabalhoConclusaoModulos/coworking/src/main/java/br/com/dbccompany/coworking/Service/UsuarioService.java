package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    public UsuarioDTO salvar(UsuarioEntity usuario) {
        BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

        usuario.setSenha(bcryptEncoder.encode(usuario.getSenha()));
        return new UsuarioDTO(repository.save(usuario));
    }

    public List<UsuarioDTO> findAll() throws Exception {
        return parseList( repository.findAll() );
    }

    public UsuarioDTO findById( Integer id ) throws Exception {
        try{
            Optional<UsuarioEntity> optionalUsuarioEntity = repository.findById(id);

            if(optionalUsuarioEntity.isEmpty())
                throw new ExceptionGeneric( "item não encontrado" );
            
            return new UsuarioDTO( optionalUsuarioEntity.get() );
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao buscar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public UsuarioDTO editar(UsuarioEntity usuarioEntity, Integer id) throws Exception {
        try{
            Optional<UsuarioEntity> optionalUsuarioEntity = repository.findById(id);

            if(optionalUsuarioEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            usuarioEntity.setId(id);
            return salvar( usuarioEntity );
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao editar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<UsuarioDTO> parseList(List<UsuarioEntity> list){
        if(list == null || list.size() == 0 ) return null;

        List<UsuarioDTO> dtos = new ArrayList<>();
        for(UsuarioEntity u : list){
            dtos.add(new UsuarioDTO( u ));
        }
        return dtos;
    }
}
