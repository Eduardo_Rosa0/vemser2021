package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    @Override
    Optional<EspacoPacoteEntity> findById(Integer integer);

    List<EspacoPacoteEntity> findAll();

    List<EspacoPacoteEntity> findAllByPacote(PacoteEntity pacote );

    List<EspacoPacoteEntity> findAllByEspaco( EspacoEntity espaco );

    List<EspacoPacoteEntity> findAllByTipoContratacao( String tipoContratacao );

    List<EspacoPacoteEntity> findAllByPrazo( Integer prazo );
}
