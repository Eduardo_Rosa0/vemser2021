package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Helper.ConversorMoeda;

import java.util.ArrayList;
import java.util.List;

public class EspacoDTO {
    private Integer id;
    private String nome;
    private Integer qntPessoas;
    private String valor;
    private List<Integer> espcoPacoteIds;
    private List<Integer> contratacoesIds;

    public EspacoDTO(EspacoEntity espaco){
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qntPessoas = espaco.getQuantidadePessoas();
        this.valor = ConversorMoeda.doubleParaString(espaco.getValor());
        this.espcoPacoteIds = parseListEspacoPacote(espaco.getPacotes());
        this.contratacoesIds = parseListContratacoes(espaco.getContratacoes());
    }

    public EspacoDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQntPessoas() {
        return qntPessoas;
    }

    public void setQntPessoas(Integer qntPessoas) {
        this.qntPessoas = qntPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<Integer> getEspcoPacoteIds() {
        return espcoPacoteIds;
    }

    public void setEspcoPacoteIds(List<Integer> espcoPacoteIds) {
        this.espcoPacoteIds = espcoPacoteIds;
    }

    public List<Integer> getContratacoesIds() {
        return contratacoesIds;
    }

    public void setContratacoesIds(List<Integer> contratacoesIds) {
        this.contratacoesIds = contratacoesIds;
    }

    public List<Integer> parseListEspacoPacote(List<EspacoPacoteEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids= new ArrayList<>();
        for( EspacoPacoteEntity espaco : list ){
            ids.add(espaco.getId());
        }

        return ids;
    }

    public List<Integer> parseListContratacoes (List<ContratacaoEntity> list) {
        if (list == null || list.size() == 0) return null;

        List<Integer> ids = new ArrayList<>();
        for (ContratacaoEntity contratacao : list) {
            ids.add(contratacao.getId());
        }
        return ids;
    }

}
