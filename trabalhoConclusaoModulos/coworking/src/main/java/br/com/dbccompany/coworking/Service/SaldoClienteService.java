package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.DTO.SaldoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;
    @Autowired
    private ClienteRepository repositoryCliente;
    @Autowired
    private EspacoRepository repositoryEspaco;

    @Transactional(rollbackOn = Exception.class)
    public SaldoDTO save(SaldoClienteEntity saldoCliente) throws Exception {
        try{
            if( saldoCliente.getCliente() != null && saldoCliente.getEspaco() != null ){
                SaldoID id = new SaldoID( saldoCliente.getCliente().getId(), saldoCliente.getEspaco().getId() );
                saldoCliente.setId(id);
                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                SaldoClienteEntity saldoClienteEntity = repository.save(saldoCliente);
                return new SaldoDTO( saldoClienteEntity );
            }
            throw new ExceptionGeneric( "Verifique os dados" );

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public SaldoDTO update(SaldoClienteEntity saldo, SaldoID id ) throws Exception {
        try{
            Optional<SaldoClienteEntity> saldoClienteEntity = repository.findById(id);
            if(saldoClienteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            saldo.setId(id);
            return save(saldo);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<SaldoDTO> findAll() throws Exception {
        try{
            List<SaldoDTO> saldoDTOList =  parseList(repository.findAll());
            if(saldoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return saldoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public SaldoDTO findById( SaldoID id ) throws Exception {
        try{
            if(id == null) throw new ExceptionGeneric( "Verifique os dados" );

            Optional<SaldoClienteEntity> saldo = repository.findById(id);

            if(saldo.isEmpty()) throw new ExceptionGeneric( "Verifique os dados" );

            return new SaldoDTO(saldo.get());
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<SaldoDTO> parseList(List<SaldoClienteEntity> list){
        if(list == null || list.size() == 0 ) return null;

        List<SaldoDTO> dtos = new ArrayList<>();
        for(SaldoClienteEntity saldo : list){
            dtos.add(new SaldoDTO(saldo));
        }
        return dtos;
    }

    public List<SaldoDTO> findAllByCliente( Integer id ) throws Exception {
        try{
            Optional<ClienteEntity> clienteEntity = repositoryCliente.findById(id);
            if(clienteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );
            List<SaldoDTO> saldoDTOList = parseList(repository.findAllByCliente( clienteEntity.get() ));
            if(saldoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return saldoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<SaldoDTO> findAllByEspaco( Integer id ) throws Exception {
        try{

            Optional<EspacoEntity> espacoEntity = repositoryEspaco.findById(id);
            if(espacoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            List<SaldoDTO> saldoDTOList = parseList(repository.findAllByEspaco( espacoEntity.get() ));
            if(saldoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return saldoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<SaldoDTO> findAllByTipoContratacao( String tipoContratacao ) throws Exception {
        try{
            List<SaldoDTO> saldoDTOList = parseList(repository.findAllByTipoContratacao( tipoContratacao ));
            if(saldoDTOList == null)
                throw new ExceptionGeneric( "Verifique os dados" );

            return saldoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }


    public SaldoClienteEntity parseEntity(SaldoDTO dto){
        if( dto.getClienteId() == null || dto.getEspacoId() == null ) return null;

        SaldoClienteEntity saldo = new SaldoClienteEntity();
        SaldoID id = new SaldoID( dto.getClienteId(), dto.getEspacoId() );

        saldo.setId(id);
        saldo.setTipoContratacao(dto.getTipoContratacao());
        saldo.setVencimento(dto.getVencimento());
        saldo.setQuantidade(dto.getQuantidade());
        saldo.setCliente(buscaCliente(dto.getClienteId()));
        saldo.setEspaco(buscaEspaco(dto.getEspacoId()));

        return saldo;
    }

    public SaldoID parseId( Integer clienteId, Integer espacoId ){
        SaldoID id = new SaldoID( clienteId, espacoId );

        boolean notExists = repository.findById(id).isEmpty();
        if (notExists) return null;

        return id;
    }

    public EspacoEntity buscaEspaco( Integer id ){
        Optional< EspacoEntity > espacoEntity = repositoryEspaco.findById(id);
        if(espacoEntity.isEmpty())
            return null;
        return espacoEntity.get();
    }

    public ClienteEntity buscaCliente( Integer id ){
        Optional< ClienteEntity > clienteEntity = repositoryCliente.findById(id);
        if(clienteEntity.isEmpty())
            return null;
        return clienteEntity.get();
    }
}
