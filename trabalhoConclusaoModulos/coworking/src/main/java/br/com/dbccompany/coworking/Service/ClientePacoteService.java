package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientePacoteService {
    @Autowired
    private ClientePacoteRepository repository;

    @Autowired
    private PagamentoRepository repositoryPagamento;

    @Autowired
    private PacoteRepository repositoryPacote;

    @Autowired
    private ClienteRepository repositoryCliente;

    @Transactional(rollbackOn = Exception.class)
    public ClientePacoteDTO save(ClientePacoteEntity clientePacote ) throws Exception {
        try{
            PacoteEntity pacote = clientePacote.getPacote() ;
            ClienteEntity cliente =  clientePacote.getCliente() ;

            if(pacote != null && cliente != null) {
                clientePacote.setCliente( cliente );
                clientePacote.setPacote( pacote );
                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                return new ClientePacoteDTO(repository.save(clientePacote));
            }

            throw new ExceptionGeneric( "São necessários um espaço e um pacote para salvar esse item!" );
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public ClientePacoteDTO update( ClientePacoteEntity cli, Integer id ) throws Exception {
        try{
            Optional<ClientePacoteEntity> clientePacoteEntity = repository.findById(id);
            if(clientePacoteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );
            cli.setId(id);
            return save(cli);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public ClientePacoteDTO findById( Integer id ) throws Exception {
        try{
            Optional<ClientePacoteEntity> cliente  = repository.findById(id);
            if(cliente.isPresent())
                return new ClientePacoteDTO(cliente.get());

            throw new ExceptionGeneric( "Item não localizado!");

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ClientePacoteDTO> findAll() throws Exception {
        try{
            List<ClientePacoteEntity> clientes = repository.findAll();
            if(clientes.size() > 0)
                return parseList(clientes);

            throw new ExceptionGeneric( "Nenhum item localizado!");

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ClientePacoteDTO> findAllByCliente( Integer id ) throws Exception {
        try{
            Optional<ClienteEntity> clienteEntity = repositoryCliente.findById(id);
            if(clienteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados");

            List<ClientePacoteEntity> clientes = repository.findAllByCliente( clienteEntity.get() );
            if(clientes.size() > 0)
                return parseList(clientes);

            throw new ExceptionGeneric( "Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ClientePacoteDTO> findAllByPacote( Integer id ) throws Exception {
        try{
            Optional<PacoteEntity> pacoteEntity = repositoryPacote.findById(id);
            if(pacoteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados");

            List<ClientePacoteEntity> clientes = repository.findAllByPacote( pacoteEntity.get() );
            if(clientes.size() > 0)
                return parseList(clientes);

            throw new ExceptionGeneric( "Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ClientePacoteDTO> parseList(List<ClientePacoteEntity> list){
        if(list == null || list.size() == 0 ) return null;

        List<ClientePacoteDTO> dtos = new ArrayList<>();
        for(ClientePacoteEntity cli : list){
            dtos.add(new ClientePacoteDTO(cli));
        }
        return dtos;
    }

    public ClientePacoteEntity parseEntity( ClientePacoteDTO dto ){
        ClientePacoteEntity clientePacoteEntity = new ClientePacoteEntity();

        if(dto.getId() != null)
            clientePacoteEntity.setId(dto.getId());

        clientePacoteEntity.setQuantidade(dto.getQuantidade());
        clientePacoteEntity.setCliente( buscaCliente(dto.getClienteId()) );
        clientePacoteEntity.setPacote( buscaPacote(dto.getPacoteId()) );

        if(buscaPagamentos(dto.getPagamentos()) != null)
            clientePacoteEntity.setPagamentos( buscaPagamentos(dto.getPagamentos()) );


        return  clientePacoteEntity;
    }

    public List<PagamentoEntity> buscaPagamentos( List<Integer> ids){
        if(ids == null) return null;
        List<PagamentoEntity> pagamentoEntityList = new ArrayList<>();
        for( Integer id : ids ){
            Optional<PagamentoEntity> pagamentoEntity = repositoryPagamento.findById(id);
            if(pagamentoEntity.isPresent())
                pagamentoEntityList.add(pagamentoEntity.get());
        }
        return pagamentoEntityList;
    }

    public ClienteEntity buscaCliente( Integer id ){
        if(id == null) return null;
        Optional<ClienteEntity> clienteEntity =  repositoryCliente.findById(id);
        if(clienteEntity.isEmpty())
            return null;

        return clienteEntity.get();
    }

    public PacoteEntity buscaPacote(Integer id){
        Optional<PacoteEntity> pacote = repositoryPacote.findById(id);
        return pacote.orElse(null);
    }
}
