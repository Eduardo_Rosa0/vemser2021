package br.com.dbccompany.coworking.Helper;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class Log {
    private static Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);
    private static RestTemplate restTemplate = new RestTemplate();
    private static String url = "http://localhost:8081/api/mangolona/save";

    public static void enviaErro500( Exception e ){
        logger.error(e.getMessage());
        ErroDTO erro = new ErroDTO( e, "ERROR", e.getMessage(), "500" );
        try{
            restTemplate.postForObject(url, erro, Object.class);
        }catch (Exception ex){
            logger.error("ATENÇÃO JOB DE PERSISTÊNCIA DE LOGS RECUSOU MINHA CHAMADA..");
        }
    }

    public static void enviaWarn403( Exception e ){
        logger.warn(e.getMessage());
        ErroDTO erro = new ErroDTO( e, "WARN", e.getMessage(), "500" );
        try{
            restTemplate.postForObject(url, erro, Object.class);
        }catch (Exception ex ){
            logger.error("ATENÇÃO JOB DE PERSISTÊNCIA DE LOGS RECUSOU MINHA CHAMADA..");
        }
    }

    public static void enviaInfo201( Exception e ){
        logger.info(e.getMessage());
        ErroDTO erro = new ErroDTO( e, "INFO", e.getMessage(), "500" );
        try{
            restTemplate.postForObject(url, erro, Object.class);
        }catch (Exception ex){
            logger.error("ATENÇÃO JOB DE PERSISTÊNCIA DE LOGS RECUSOU MINHA CHAMADA..");
        }
    }
}
