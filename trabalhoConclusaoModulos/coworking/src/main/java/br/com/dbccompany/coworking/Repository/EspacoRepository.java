package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    @Override
    Optional<EspacoEntity> findById( Integer integer );

    EspacoEntity findByNome( String nome );

    List<EspacoEntity> findAll();

    List<EspacoEntity> findAllByQuantidadePessoas(Integer quantidadePessoas );

    List<EspacoEntity> findAllByValor(Double valor );
}
