package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping (value = "/api/cw/tipos/contato")
public class TipoContatoController {

    @Autowired
    private TipoContatoService service;

    @PostMapping (value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarTipoContato(@RequestBody TipoContatoDTO tipo){
        try{
            return new ResponseEntity<>( service.salvar( service.parseEntity(tipo) ), HttpStatus.CREATED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarTipoContato(@RequestBody TipoContatoDTO tipo, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar( service.parseEntity(tipo) , id), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<Object>  findAll(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public ResponseEntity<Object> findByNome(@PathVariable String nome){
        try{
            return new ResponseEntity<>(service.findByNome(nome), HttpStatus.OK);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
