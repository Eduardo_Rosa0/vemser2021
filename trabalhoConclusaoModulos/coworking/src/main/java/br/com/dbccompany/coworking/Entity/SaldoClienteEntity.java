package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "SALDO")
public class SaldoClienteEntity {

    @EmbeddedId
    private SaldoID id;

    @ManyToOne
    @MapsId("id_cliente")
    private ClienteEntity cliente;

    @ManyToOne
    @MapsId("id_espaco")
    private EspacoEntity espaco;

    private TipoContratacaoEnum tipoContratacao;

    private Integer quantidade;

    private LocalDate vencimento;

    public SaldoID getId() {
        return id;
    }

    public void setId(SaldoID id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }
}
