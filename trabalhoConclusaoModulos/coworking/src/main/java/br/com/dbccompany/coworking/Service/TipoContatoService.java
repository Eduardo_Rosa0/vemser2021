package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Helper.ParseList;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;
    @Autowired
    private ContatoRepository repositoryContato;

    @Transactional(rollbackOn = Exception.class)
    public TipoContatoDTO salvar(TipoContatoEntity tipoContato) throws Exception {
        try{
            if(repository.findByNome( tipoContato.getNome() ) == null) {
                TipoContatoEntity tipoContatoEntity = repository.save(tipoContato);
                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                return new TipoContatoDTO(tipoContatoEntity);
            }
            throw new ExceptionGeneric("Verifique os dados");

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public TipoContatoDTO editar(TipoContatoEntity tipoContato, Integer id) throws Exception {
        try{
            Optional<TipoContatoEntity> tipoContatoEntity = repository.findById(id);

            if(tipoContatoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            tipoContato.setId(id);
            return salvar(tipoContato);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao editar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public TipoContatoDTO findById(Integer id) throws Exception {
        try{
            Optional<TipoContatoEntity> tipo = repository.findById(id);
            if(tipo.isPresent())
                return new TipoContatoDTO( tipo.get() );

            throw new ExceptionGeneric("Item não localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<TipoContatoDTO> findAll() throws Exception {
        try{
            List<TipoContatoDTO> tipoContatoDTOList = ParseList.<TipoContatoEntity, TipoContatoDTO>parseEntitiesToDtos(repository.findAll(), TipoContatoDTO.class);
            if( tipoContatoDTOList == null)
                throw new ExceptionGeneric("Verifique os dados");
            return tipoContatoDTOList;
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public TipoContatoDTO findByNome(String nome) throws Exception {
        try{
            TipoContatoEntity tipoContatoEntity = repository.findByNome(nome);
            if(tipoContatoEntity == null)
                throw new ExceptionGeneric("Verifique os dados");
            return new TipoContatoDTO(tipoContatoEntity);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }


    public TipoContatoEntity parseEntity(TipoContatoDTO dto ){
        TipoContatoEntity tipo = new TipoContatoEntity();
        List<ContatoEntity> ctts = getContatos(dto.getContatos());

        tipo.setId(dto.getId());
        tipo.setNome(dto.getNome());

        if(ctts != null)
            tipo.setContatos(ctts);

        return tipo;
    }

    public List<ContatoEntity> getContatos( List<Integer> ids ){
        List<ContatoEntity> clientes = new ArrayList<>();
        if(ids == null || ids.size() == 0) return null;

        for( Integer id : ids ){
            Optional<ContatoEntity> ctt = repositoryContato.findById(id);
            if(ctt.isPresent())
                clientes.add( ctt.get() );
        }

        return  clientes;
    }



}
