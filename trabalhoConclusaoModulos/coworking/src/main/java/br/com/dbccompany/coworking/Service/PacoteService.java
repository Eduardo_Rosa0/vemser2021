package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.*;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.ConversorMoeda;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PacoteService {

    @Autowired
    private PacoteRepository repository;

    @Autowired
    private EspacoPacoteRepository repositoryEspacoPacote;

    @Autowired
    private EspacoRepository repositoryEspaco;

    @Autowired
    private ClientePacoteRepository repositoryClientes;

    @Transactional(rollbackOn = Exception.class)
    public PacoteDTO salvar(PacoteEntity pacote) throws Exception {
        try{
                PacoteEntity espacoSalvo = repository.save(pacote);
                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                return new PacoteDTO(espacoSalvo);

        } catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public PacoteDTO editar(PacoteEntity pacote, Integer id) throws Exception {
        try{
            if(!repository.findById(id).isPresent())
                throw new ExceptionGeneric( "Item não localizado" );

            pacote.setId(id);

            return new PacoteDTO(repository.save(pacote));
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }


    public PacoteDTO buscarPorId(Integer id) throws Exception {
        try{
            Optional<PacoteEntity> pacoteEntity = repository.findById(id);

            if(pacoteEntity.isPresent())
                return new PacoteDTO( pacoteEntity.get() );

            throw new ExceptionGeneric("Item não localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PacoteDTO> buscarTodos() throws Exception {
        try{
            List<PacoteDTO> pacoteEntityList = parseList(repository.findAll());

            if( pacoteEntityList != null )
                return pacoteEntityList;

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PacoteDTO> buscarTodosPorValor(Double valor) throws Exception {
        try{
            List<PacoteDTO> pacoteEntityList = parseList(repository.findAllByValor(valor));

            if( pacoteEntityList != null )
                return pacoteEntityList;

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ClienteDTO> buscarTodosClientes( Integer pacoteId ) throws Exception {
        try{
            Optional<PacoteEntity> pacoteEntity = repository.findById(pacoteId);
            if(pacoteEntity.isEmpty()) return null;

            List<ClienteEntity> clienteEntityList = new ArrayList<>();
            for( ClientePacoteEntity clientePacoteEntity : pacoteEntity.get().getClientes() ){
                clienteEntityList.add( clientePacoteEntity.getCliente() );
            }

            if(clienteEntityList.size() > 0)
                return parseListClientes(clienteEntityList);

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    };

    public List<PagamentoDTO> buscarTodosPagamentos( Integer pacoteId ) throws Exception {
        try{
            Optional<PacoteEntity> pacoteEntity = repository.findById(pacoteId);
            if(pacoteEntity.isEmpty()) return null;

            List<PagamentoEntity> pagamentoEntityList = new ArrayList<>();
            for( ClientePacoteEntity clientePacoteEntity : pacoteEntity.get().getClientes()){
                pagamentoEntityList.addAll(clientePacoteEntity.getPagamentos());
            }

            if(pagamentoEntityList.size() > 0)
                return parseListPagamentos(pagamentoEntityList);

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    };

    public List<EspacoDTO> buscarTodosEspacos( Integer pacoteId ) throws Exception {
        try{
            Optional<PacoteEntity> pacoteEntity = repository.findById(pacoteId);
            if(pacoteEntity.isEmpty()) return null;

            List<EspacoEntity> espacoEntityList = new ArrayList<>();
            for( EspacoPacoteEntity espacoPacoteEntity : pacoteEntity.get().getEspacos() ){
                espacoEntityList.add( espacoPacoteEntity.getEspaco() );
            }

            if(espacoEntityList.size() > 0)
                return parseListEspacos(espacoEntityList);

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    };

    public List<PacoteDTO> parseList(List<PacoteEntity> list){
        if( list == null || list.size() == 0) return null;

        List<PacoteDTO> dtos = new ArrayList<>();
        for(PacoteEntity pct : list){
            dtos.add(new PacoteDTO(pct));
        }
        return dtos;
    }

    public List<EspacoDTO> parseListEspacos(List<EspacoEntity> list){
        if( list == null || list.size() == 0) return null;

        List<EspacoDTO> dtos = new ArrayList<>();
        for(EspacoEntity e : list){
            dtos.add(new EspacoDTO(e));
        }
        return dtos;
    }

    public List<ClienteDTO> parseListClientes(List<ClienteEntity> list){
        if( list == null || list.size() == 0) return null;

        List<ClienteDTO> dtos = new ArrayList<>();
        for(ClienteEntity e : list){
            dtos.add(new ClienteDTO(e));
        }
        return dtos;
    }

    public List<PagamentoDTO> parseListPagamentos(List<PagamentoEntity> list){
        if( list == null || list.size() == 0) return null;

        List<PagamentoDTO> dtos = new ArrayList<>();
        for(PagamentoEntity e : list){
            dtos.add(new PagamentoDTO(e));
        }
        return dtos;
    }

    public PacoteEntity parseEntity(PacoteDTO dto){
        PacoteEntity pacote = new PacoteEntity();

        pacote.setId(dto.getId());
        pacote.setValor(ConversorMoeda.stringParaDouble(dto.getValor()));
        pacote.setEspacos(getEspacos(dto.getEspacoPacoteIds()));
        pacote.setClientes(getClientes(dto.getClientePacoteIds()));

        return pacote;
    }

    public List<EspacoPacoteEntity> getEspacos( List<Integer> ids){
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        if(ids == null || ids.size() == 0) return null;

        for( Integer id : ids ){
            espacos.add( repositoryEspacoPacote.findById(id).get() );
        }
        return  espacos;
    }

    public List<ClientePacoteEntity> getClientes( List<Integer> ids){
        List<ClientePacoteEntity> clientes = new ArrayList<>();
        if(ids == null || ids.size() == 0) return null;

        for( Integer id : ids ){
            clientes.add( repositoryClientes.findById(id).get() );
        }

        return  clientes;
    }



}
