package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "TIPO_CONTATO")
public class TipoContatoEntity {
    @Id
    @SequenceGenerator( name = "TIPO_CTT_SEQ", sequenceName = "TIPO_CTT_SEQ" )
    @GeneratedValue( generator = "TIPO_CTT_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToMany( mappedBy = "tipo", fetch = FetchType.LAZY )
    private List<ContatoEntity> contatos;

    @Column(unique = true)
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
