package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "CLIENTES")
public class ClienteEntity {

    @Id
    @SequenceGenerator( name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ" )
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String nome;

    @Column(nullable = false, unique = true)
    private Character[] cpf = new Character[11];

    private LocalDate dataNascimento;

    @OneToMany( fetch = FetchType.LAZY, mappedBy = "cliente" )
    private List<ContatoEntity> contatos;

    @OneToMany( mappedBy = "cliente")
    private List<ClientePacoteEntity> pacotes;

    @OneToMany( mappedBy = "cliente")
    private List<ContratacaoEntity> contratacoes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character[] getCpf() {
        return cpf;
    }

    public void setCpf(Character[] cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<ClientePacoteEntity> getPacotes() {
        return pacotes;
    }

    public void setPacotes(List<ClientePacoteEntity> pacotes) {
        this.pacotes = pacotes;
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
