package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository  extends CrudRepository<ClienteEntity,Integer> {
    @Override
    Optional<ClienteEntity> findById(Integer integer);

    List<ClienteEntity> findAll();

    ClienteEntity findByNome(String nome);
    ClienteEntity findByCpf(Character[] cpf);

    List<ClienteEntity> findAllByDataNascimento(LocalDate data);

}
