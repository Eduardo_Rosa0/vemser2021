package br.com.dbccompany.coworking.Exception;

public class ExceptionGeneric extends Exception{
    private String message;

    public ExceptionGeneric(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
