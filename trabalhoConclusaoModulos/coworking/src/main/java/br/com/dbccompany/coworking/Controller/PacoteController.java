package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(value = "/api/cw/pacote")
public class PacoteController {

    @Autowired
    private PacoteService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvar(@RequestBody PacoteDTO pacote){
        try{
            return new ResponseEntity<>( service.salvar(service.parseEntity(pacote)) ,  HttpStatus.CREATED );
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    private ResponseEntity<Object>  editar(@RequestBody PacoteDTO pacote, @PathVariable Integer id){
        try{
            return new ResponseEntity<>( service.editar(service.parseEntity(pacote), id) ,  HttpStatus.OK );
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    private ResponseEntity<Object>  buscarTodos(){
        try{
            return new ResponseEntity<>( service.buscarTodos() ,  HttpStatus.OK );
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    private ResponseEntity<Object>  buscarPorId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>( service.buscarPorId(id) ,  HttpStatus.OK );
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/valor")
    @ResponseBody
    private ResponseEntity<Object>  buscarPorValor(@RequestBody Double valor){
        try{
            return new ResponseEntity<>( service.buscarTodosPorValor(valor) ,  HttpStatus.OK );
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/clientes")
    @ResponseBody
    private ResponseEntity<Object>  buscarTodosClientes(@PathVariable Integer pacoteId){
        try{
            return new ResponseEntity<>( service.buscarTodosClientes(pacoteId) ,  HttpStatus.OK );
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/pagamentos")
    @ResponseBody
    private ResponseEntity<Object>  buscarTodosPagamentos(@PathVariable Integer pacoteId){
        try{
            return new ResponseEntity<>( service.buscarTodosPagamentos(pacoteId) ,  HttpStatus.OK );
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/espacos")
    @ResponseBody
    private ResponseEntity<Object>  buscarTodosEspacos(@PathVariable Integer pacoteId){
        try{
            return new ResponseEntity<>( service.buscarTodosEspacos(pacoteId) ,  HttpStatus.OK );
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
