package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    @Override
    Optional<ContatoEntity> findById( Integer integer );

    List<ContatoEntity> findAllByTipo(TipoContatoEntity tipo );

    List<ContatoEntity> findAll();

    List<ContatoEntity> findAllByDescricao(String descricao );

    List<ContatoEntity> findAllByCliente( ClienteEntity cliente );
}
