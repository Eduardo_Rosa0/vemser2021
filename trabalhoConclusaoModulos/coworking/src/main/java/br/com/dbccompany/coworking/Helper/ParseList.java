package br.com.dbccompany.coworking.Helper;

import java.util.ArrayList;
import java.util.List;

public class ParseList {

    public static <E, D> List<D> parseEntitiesToDtos(List<E> entities, Class <D> dto) throws Exception {
        if(entities == null || entities.size() == 0) return null;
        List<D> dtos = new ArrayList<>();

        for ( E e: entities ){
            dtos.add( dto.getDeclaredConstructor(e.getClass()).newInstance(e) );
        }

       return dtos;
    }

}


