package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoID;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

import java.time.LocalDate;

public class SaldoDTO {
    private Integer clienteId;
    private Integer espacoId;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;

    public SaldoDTO() {
    }

    public SaldoDTO(SaldoClienteEntity saldo) {
        this.clienteId = saldo.getCliente().getId();
        this.espacoId = saldo.getEspaco().getId();
        this.tipoContratacao = saldo.getTipoContratacao();
        this.quantidade = saldo.getQuantidade();
        this.vencimento = saldo.getVencimento();
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer espacoId) {
        this.espacoId = espacoId;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

}
