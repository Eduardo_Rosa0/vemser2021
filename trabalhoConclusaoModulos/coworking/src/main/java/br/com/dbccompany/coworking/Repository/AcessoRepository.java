package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {
    @Override
    Optional<AcessoEntity> findById( Integer integer );

    List<AcessoEntity> findAll();

    AcessoEntity findBySaldoCliente( SaldoClienteEntity saldoCliente );

    List<AcessoEntity> findAllByEntrada( Boolean entrada );

    List<AcessoEntity> findAllBySaldoClienteAndEntradaOrderByData (SaldoClienteEntity saldoCliente, Boolean is_entrada);
}
