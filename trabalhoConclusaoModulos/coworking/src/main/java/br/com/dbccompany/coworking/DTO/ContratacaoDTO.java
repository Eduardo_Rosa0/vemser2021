package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

import java.util.ArrayList;
import java.util.List;

public class ContratacaoDTO {

    private Integer id;
    private Integer espacoId;
    private Integer clienteId;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;
    private String valor;
    private List<Integer> pagamentosId;

    public ContratacaoDTO(ContratacaoEntity contratacao) {
        this.id = contratacao.getId();
        this.espacoId = contratacao.getEspaco().getId();
        this.clienteId = contratacao.getCliente().getId();
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        this.pagamentosId = parsePagamentos( contratacao.getPagamentos() );
    }

    public ContratacaoDTO(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer espacoId) {
        this.espacoId = espacoId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<Integer> getPagamentosId() {
        return pagamentosId;
    }

    public void setPagamentosId(List<Integer> pagamentosId) {
        this.pagamentosId = pagamentosId;
    }

    public List<Integer> parsePagamentos (List<PagamentoEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids = new ArrayList<>();
        for( PagamentoEntity pagamentoEntity : list ){
            ids.add( pagamentoEntity.getId() );
        }

        return ids;
    }
}
