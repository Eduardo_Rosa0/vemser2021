package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Service.ContatoService;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(value = "/api/cw/contato")
public class ContatoController {

    @Autowired
    private ContatoService service;
    @Autowired
    private TipoContatoService serviceTipo;


    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarContato(@RequestBody ContatoDTO contato){
        try{
            return new ResponseEntity<>( service.salvar(service.parseEntity(contato)) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarContato(@RequestBody ContatoDTO contato, @PathVariable Integer id){
        try{
            return new ResponseEntity<>( service.editar( service.parseEntity(contato), id) ,HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<Object> findAll(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping (value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.findById(id), HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping (value = "/tipo")
    @ResponseBody
    public ResponseEntity<Object> findAllByTipo(@RequestBody TipoContatoDTO tipo){
        try{
            return new ResponseEntity<>(service.findAllByTipo(serviceTipo.parseEntity(tipo)), HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping (value = "/descricao/{descricao}")
    @ResponseBody
    public ResponseEntity<Object> findAllByDescricao(@PathVariable String descricao){
        try{
            return new ResponseEntity<>(service.findAllByDescricao(descricao), HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
