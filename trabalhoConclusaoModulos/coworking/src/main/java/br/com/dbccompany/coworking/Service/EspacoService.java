package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.*;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.ConversorMoeda;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;
    @Autowired
    private ContratacaoRepository repositoryContratacao;
    @Autowired
    private EspacoPacoteRepository repositoryPacote;


    @Transactional(rollbackOn = Exception.class)
    public EspacoDTO salvar(EspacoEntity espaco) throws Exception {
        try{
            if(repository.findByNome( espaco.getNome() ) == null ) {
                EspacoEntity espacoSalvo = repository.save(espaco);
                Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                return new EspacoDTO(espacoSalvo);
            }
            throw new ExceptionGeneric("Verifique os dados");

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public EspacoDTO editar(EspacoEntity espaco, Integer id) throws Exception {
        try{
            Optional<EspacoEntity> espacoEntity = repository.findById(id);
            if(espacoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            espaco.setId(id);
            return salvar(espaco);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public EspacoDTO findById(Integer id) throws Exception {
        try{
            Optional<EspacoEntity> espacoEntity = repository.findById(id);

            if(espacoEntity.isPresent())
                return new EspacoDTO( espacoEntity.get() );

            throw new ExceptionGeneric("Item não localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoDTO> findAll() throws Exception {
        try{
            List<EspacoDTO> espacoEntityList = parseList(repository.findAll());

            if( espacoEntityList != null )
                return espacoEntityList;

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public EspacoDTO findByNome(String nome) throws Exception {
        try{
            EspacoEntity espacoEntity = repository.findByNome(nome);
            if(espacoEntity != null)
                return new EspacoDTO( espacoEntity );

            throw new ExceptionGeneric("Item não localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoDTO> findAllByQuantidadePessoas( Integer qtdPassoas ) throws Exception {
        try{

            List<EspacoDTO> espacoEntityList = parseList(repository.findAllByQuantidadePessoas(qtdPassoas));

            if( espacoEntityList != null )
                return espacoEntityList;

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoDTO> findAllByValor( String valor ) throws Exception {
        try{
            Double valorConvertido = ConversorMoeda.stringParaDouble(valor);
            if(valorConvertido == null) return null;

            List<EspacoDTO> espacoEntityList = parseList(repository.findAllByValor(valorConvertido));

            if( espacoEntityList != null )
                return espacoEntityList;

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PagamentoDTO> buscarTodosPagamentos( Integer espacoid) throws Exception {
        try{
            Optional<EspacoEntity> espacoEntity = repository.findById(espacoid);
            if(espacoEntity.isEmpty()) return null;
            List<PagamentoEntity> pagamentos = new ArrayList<>();

            for( ContratacaoEntity contratacaoEntity : espacoEntity.get().getContratacoes() ){
                pagamentos.addAll(contratacaoEntity.getPagamentos());
            }

            if(pagamentos.size() > 0)
                return parseListPagamentos(pagamentos);

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<PacoteDTO> buscarTodosPacotes(Integer espacoid) throws Exception {
        try{
            Optional<EspacoEntity> espacoEntity = repository.findById(espacoid);
            if(espacoEntity.isEmpty()) return null;

            List<PacoteEntity> pacoteEntityList = new ArrayList<>();
            for( EspacoPacoteEntity espacoPacoteEntity : espacoEntity.get().getPacotes()  ){
                pacoteEntityList.add( espacoPacoteEntity.getPacote() );
            }

            if(pacoteEntityList.size() > 0)
                return parseListPacotes(pacoteEntityList);

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao consultar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<EspacoDTO> parseList(List<EspacoEntity> list){
        if( list == null || list.size() == 0) return null;

        List<EspacoDTO> dtos = new ArrayList<>();
        for(EspacoEntity espaco : list){
            dtos.add(new EspacoDTO(espaco));
        }
        return dtos;
    }

    public List<PagamentoDTO> parseListPagamentos(List<PagamentoEntity> list){
        if( list == null || list.size() == 0) return null;

        List<PagamentoDTO> dtos = new ArrayList<>();
        for(PagamentoEntity e : list){
            dtos.add(new PagamentoDTO(e));
        }
        return dtos;
    }

    public List<PacoteDTO> parseListPacotes(List<PacoteEntity> list){
        if( list == null || list.size() == 0) return null;

        List<PacoteDTO> dtos = new ArrayList<>();
        for(PacoteEntity e : list){
            dtos.add(new PacoteDTO(e));
        }
        return dtos;
    }

    public EspacoEntity parseEntity(EspacoDTO dto){
        EspacoEntity espaco = new EspacoEntity();

        espaco.setId(dto.getId());
        espaco.setNome(dto.getNome());
        espaco.setValor(ConversorMoeda.stringParaDouble(dto.getValor()));
        espaco.setQuantidadePessoas(dto.getQntPessoas());
        espaco.setContratacoes(getContratacoes(dto.getContratacoesIds()));
        espaco.setPacotes(getPacotes(dto.getEspcoPacoteIds()));

        return espaco;
    }

    public List<ContratacaoEntity> getContratacoes( List<Integer> ids){
        List<ContratacaoEntity> clientes = new ArrayList<>();
        if(ids == null || ids.size() == 0) return null;

        for( Integer id : ids ){
            clientes.add( repositoryContratacao.findById(id).get() );
        }

        return  clientes;
    }

    public List<EspacoPacoteEntity> getPacotes(List<Integer> ids){
        List<EspacoPacoteEntity> pacotes = new ArrayList<>();
        if(ids == null || ids.size() == 0) return null;

        for( Integer id : ids ){
            pacotes.add( repositoryPacote.findById(id).get() );
        }

        return  pacotes;
    }

}
