package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;
    @Autowired
    private TipoContatoRepository repositoryTipo;
    @Autowired
    private ContatoRepository repositoryContato;
    @Autowired
    private ContratacaoRepository repositoryContratacao;
    @Autowired
    private ClientePacoteRepository repositoryPacote;

    @Transactional(rollbackOn = Exception.class)
    public ClienteDTO save( ClienteEntity cliente ) throws Exception {
        try{
            if(contatosOK(cliente)){
                if( null == repository.findByCpf(cliente.getCpf()) ) {
                    ClienteEntity clienteSalvo = repository.save(cliente);


                    for (ContatoEntity contato : clienteSalvo.getContatos()) {
                        if (contato.getCliente() != null)
                            throw new ExceptionGeneric(" Verifique os dados ");
                        contato.setCliente(clienteSalvo);
                        repositoryContato.save(contato);
                    }

                    Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
                    return new ClienteDTO(clienteSalvo);
                }
            }
            throw new ExceptionGeneric(" Verifique os dados ");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public ClienteDTO update(ClienteEntity cliente, Integer id) throws Exception {
        try{
            Optional<ClienteEntity> clienteEntity = repository.findById(id);
            if(clienteEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            cliente.setId(id);
            return save(cliente);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<ClienteDTO> findAll() throws Exception {
        try{
            List<ClienteEntity> clientes = repository.findAll();

            if(clientes.size() > 0)
                return parseList(clientes);

            throw new ExceptionGeneric( "Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public ClienteDTO findById(Integer id) throws Exception {
        try{
            Optional<ClienteEntity> cliente  = repository.findById(id);

            if(cliente.isPresent())
                return new ClienteDTO(cliente.get());

            throw new ExceptionGeneric( "Item não localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public ClienteDTO findByNome(String nome) throws Exception {
        try{
            ClienteEntity cliente  = repository.findByNome(nome);
            if(cliente != null)
                return new ClienteDTO(cliente);

            throw new ExceptionGeneric( "Item não localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public ClienteDTO findByCpf(String cpf) throws Exception {
        try{
            ClienteEntity cliente  = repository.findByCpf(new ClienteDTO().converteParaChar(cpf));
            if(cliente != null)
                return new ClienteDTO(cliente);

            throw new ExceptionGeneric( "Item não localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }


    public List<ClienteDTO> findAllByDataNascimento(Date data) throws Exception {
        try{
            List<ClienteEntity> clientes = repository.findAllByDataNascimento(data.toInstant().atZone( ZoneId.systemDefault() ).toLocalDate());

            if(clientes.size() > 0)
                return parseList(clientes);

            throw new ExceptionGeneric( "Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }


    public List<ClienteDTO> parseList(List<ClienteEntity> list){
        if(list == null || list.size() == 0 ) return null;

        List<ClienteDTO> dtos = new ArrayList<>();
        for(ClienteEntity cli : list){
            dtos.add(new ClienteDTO(cli));
        }
        return dtos;
    }

    public Boolean contatosOK(ClienteEntity cliente){
        Integer emailOK = 0;
        Integer foneOK = 0;
        List<ContatoEntity> contatos = cliente.getContatos();
        TipoContatoEntity emailType = repositoryTipo.findByNome("email");
        TipoContatoEntity foneType = repositoryTipo.findByNome("telefone");

        if( contatos == null || contatos.size() == 0 ) return false;

        for( ContatoEntity contato : contatos ){
            if(contato.getTipo().getNome().equalsIgnoreCase(emailType.getNome()))
                emailOK++;
            if(contato.getTipo().getNome().equalsIgnoreCase(foneType.getNome()))
                foneOK++;
        }

        return emailOK >= 1 && foneOK >= 1;
    }

    public ClienteEntity parseEntity(ClienteDTO dto){
        ClienteEntity cli = new ClienteEntity();

        List<ContatoEntity> contatos = getContatos(dto.getContatos());
        List<ClientePacoteEntity> pacotes = getPacotes(dto.getPacotes());
        List<ContratacaoEntity> contratacoes = getContratacoes(dto.getContratacoes());

        cli.setId(dto.getId());
        cli.setNome(dto.getNome());
        cli.setCpf(dto.converteParaChar(dto.getCpf()));
        cli.setDataNascimento(dto.getDataNascimento());

        if(contatos != null && contatos.size() > 0)
            cli.setContatos(contatos);
        if(contratacoes != null && contratacoes.size() > 0)
            cli.setContratacoes(contratacoes);
        if(pacotes != null && pacotes.size() > 0)
            cli.setPacotes(pacotes);

        return cli;
    }

    public List<ClientePacoteEntity> getPacotes(List<Integer> ids){
        List<ClientePacoteEntity> pacotes = new ArrayList<>();

        if(ids == null || ids.size() == 0) return null;

        for( Integer id : ids ){
            Optional<ClientePacoteEntity> pacote = repositoryPacote.findById(id);
            if(pacote.isPresent())
                pacotes.add( pacote.get() );
        }
        return pacotes;
    }

    public List<ContratacaoEntity> getContratacoes(List<Integer> ids){
        List<ContratacaoEntity> contratacoes = new ArrayList<>();

        if(ids == null || ids.size() == 0) return null;

        for( Integer id : ids ){
            Optional<ContratacaoEntity> contratacao = repositoryContratacao.findById(id);
            if(contratacao.isPresent())
                contratacoes.add(contratacao.get());
        }
        return contratacoes;
    }

    public List<ContatoEntity> getContatos(List<Integer> ids ){
        List<ContatoEntity> ctts = new ArrayList<>();

        if(ids == null || ids.size() == 0) return null;

        for( Integer id : ids ) {
            Optional<ContatoEntity> ctt = repositoryContato.findById(id);
            if (ctt.isPresent()) {
                ctts.add(ctt.get());
            }
        }
        return ctts;
    }
}
