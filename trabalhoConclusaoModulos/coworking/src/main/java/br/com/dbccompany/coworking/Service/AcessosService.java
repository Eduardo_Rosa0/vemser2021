package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Helper.Log;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    private AcessoRepository repository;
    @Autowired
    private SaldoClienteRepository repositorySaldo;
    @Autowired
    private ClienteRepository repositoryCliente;
    @Autowired
    private EspacoRepository repositoryEspaco;

    @Transactional(rollbackOn = Exception.class)
    public AcessoDTO save(AcessoEntity acesso) throws Exception {
        try{
            SaldoClienteEntity saldo = acesso.getSaldoCliente();

            if(saldo == null)
                throw new ExceptionGeneric( "É necessário um saldo para este item ser salvo" );

            acesso.setSaldoCliente(repositorySaldo.save(saldo));
            Log.enviaInfo201(new ExceptionGeneric("Um novo Item foi criado!"));
            return new AcessoDTO( repository.save( acesso ) );

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public AcessoDTO update(AcessoEntity acesso, Integer id ) throws Exception {
        try{
            Optional<AcessoEntity> acessoEntity = repository.findById(id);
            if(acessoEntity.isEmpty())
                throw new ExceptionGeneric( "Verifique os dados" );

            acesso.setId(id);
            return save(acesso);
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public AcessoDTO findById( Integer id ) throws Exception {
        try{
            Optional<AcessoEntity> acesso = repository.findById(id);
            if(acesso.isPresent())
                return new AcessoDTO( acesso.get() );

            throw new ExceptionGeneric( "Item não localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<AcessoDTO> findAll() throws Exception {
        try{
            List<AcessoEntity> acessos = repository.findAll();
            if(acessos.size() > 0)
                return parseList(acessos);
            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public AcessoDTO findBySaldoCliente( SaldoClienteEntity saldoCliente ) throws Exception {
        try{
            AcessoEntity acesso = repository.findBySaldoCliente( saldoCliente );
            if(acesso != null)
                return new AcessoDTO(acesso);

            throw new ExceptionGeneric( "Item não localizado!");

        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<AcessoDTO> findAllByEntrada( Boolean entrada ) throws Exception {
        try{
            List<AcessoEntity> acessos = repository.findAllByEntrada( entrada );
            if(acessos.size() > 0)
                return parseList(acessos);

            throw new ExceptionGeneric("Nenhum item localizado!");
        }catch (ExceptionGeneric e){
            Log.enviaWarn403(e);
            throw new ExceptionGeneric( "Ocorreu um problema ao salvar: [ " + e.getMessage() + " ]");
        }catch (Exception e){
            Log.enviaErro500(e);
            throw new Exception(e.getMessage());
        }
    }

    public List<AcessoDTO> parseList(List<AcessoEntity> list){
        if(list == null || list.size() == 0 ) return null;

        List<AcessoDTO> dtos = new ArrayList<>();
        for(AcessoEntity saldo : list){
            dtos.add(new AcessoDTO(saldo));
        }
        return dtos;
    }

    public AcessoEntity parseEntity(AcessoDTO dto ){
        AcessoEntity acesso = new AcessoEntity();
        SaldoClienteEntity saldo = getSaldoCliente(dto.getClienteId(), dto.getEspacoId());

        acesso.setEntrada(dto.getEntrada());
        acesso.setData(dto.getData());
        acesso.setExcecao(dto.getExcessao());
        if(saldo != null)
            acesso.setSaldoCliente(saldo);

        return acesso;
    }

    public SaldoClienteEntity getSaldoCliente(Integer idCliente, Integer idEsapco){
        Optional<ClienteEntity> cliente = repositoryCliente.findById(idCliente);
        Optional<EspacoEntity> espaco = repositoryEspaco.findById(idEsapco);

        if(cliente.isPresent() && espaco.isPresent() )
            return repositorySaldo.findByClienteAndEspaco( cliente.get(), espaco.get() );

        return null;
    }

}
