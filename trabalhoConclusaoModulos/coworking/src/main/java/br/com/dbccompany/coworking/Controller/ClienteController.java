package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Exception.ExceptionGeneric;
import br.com.dbccompany.coworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@RequestMapping(value = "/api/cw/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @PostMapping (value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> saveCliente(@RequestBody ClienteDTO cliente){
        try{
            return new ResponseEntity<>( service.save(service.parseEntity(cliente)) , HttpStatus.CREATED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> updateCliente(@RequestBody ClienteDTO cliente, @PathVariable Integer id){
        try{
            return new ResponseEntity<>( service.update(service.parseEntity(cliente), id) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<Object> findAll(){
        try{
            return new ResponseEntity<>( service.findAll() , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping (value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>( service.findById(id) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping (value = "/nome/{nome}")
    @ResponseBody
    public ResponseEntity<Object> findByNome(@PathVariable String nome){
        try{
            return new ResponseEntity<>( service.findByNome(nome) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping (value = "/cpf/{cpf}")
    @ResponseBody
    public ResponseEntity<Object> findByCpf(@PathVariable String cpf){
        try{
            return new ResponseEntity<>( service.findByCpf(cpf) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/dataNascimento/")
    @ResponseBody
    public ResponseEntity<Object> findAllByDataNascimento(@RequestBody Date data){
        try{
            return new ResponseEntity<>( service.findAllByDataNascimento(data) , HttpStatus.ACCEPTED);
        }catch (ExceptionGeneric e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (Exception e ){
            return new ResponseEntity<>( "Ocorreu um problema interno, contate o suporte" , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
