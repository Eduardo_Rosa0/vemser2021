package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoID> {
    @Override
    Optional<SaldoClienteEntity> findById( SaldoID saldoID );

    List<SaldoClienteEntity> findAll(   );

    List<SaldoClienteEntity> findAllByCliente( ClienteEntity cliente );

    List<SaldoClienteEntity> findAllByEspaco( EspacoEntity espaco );

    List<SaldoClienteEntity> findAllByTipoContratacao( String tipoContratacao );

    SaldoClienteEntity findByClienteAndEspaco( ClienteEntity cliente, EspacoEntity espaco );
}
