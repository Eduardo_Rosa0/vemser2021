package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;

import java.util.ArrayList;
import java.util.List;

public class ClientePacoteDTO {
    private Integer id;
    private Integer clienteId;
    private Integer pacoteId;
    private Integer quantidade;
    private List<Integer> pagamentos;

    public ClientePacoteDTO(ClientePacoteEntity clientePacote) {
        this.id = clientePacote.getId();
        this.clienteId = clientePacote.getCliente().getId();
        this.pacoteId = clientePacote.getPacote().getId();
        this.quantidade = clientePacote.getQuantidade();
        this.pagamentos = parseListPagamentos(clientePacote.getPagamentos());
    }

    public ClientePacoteDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getPacoteId() {
        return pacoteId;
    }

    public void setPacoteId(Integer pacoteId) {
        this.pacoteId = pacoteId;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public List<Integer> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Integer> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public List<Integer> parseListPagamentos (List<PagamentoEntity> list) {
        if(list == null || list.size() == 0) return null;

        List<Integer> ids = new ArrayList<>();
        for( PagamentoEntity pagto : list ){
            ids.add( pagto.getId() );
        }
        return ids;
    }
}
