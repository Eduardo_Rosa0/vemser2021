package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class TipoContatoTest {

    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void salve(){
        TipoContatoEntity tipoFone = new TipoContatoEntity();
        tipoFone.setNome("telefone");

        TipoContatoEntity tipoEmail = new TipoContatoEntity();
        tipoEmail.setNome("email");

        repository.save(tipoFone);
        repository.save(tipoEmail);

        assertTrue( repository.findAll().size() == 2 );
    }

}
