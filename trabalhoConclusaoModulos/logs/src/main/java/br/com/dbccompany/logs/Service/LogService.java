package br.com.dbccompany.logs.Service;

import br.com.dbccompany.logs.DTO.LogDTO;
import br.com.dbccompany.logs.Entity.LogEntity;
import br.com.dbccompany.logs.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LogService {

    @Autowired
    private LogRepository repository;

    public LogDTO findById( String id ){
        Optional<LogEntity> log = repository.findById(id);
        return log.isPresent()
                ? new LogDTO(log.get())
                : null;
    }

    public List<LogDTO> findAll(){
        return listDTOParse(repository.findAll());
    }

    public LogDTO save( LogEntity log ){
        return new LogDTO(repository.save(log));
    }

    public List<LogDTO> findAllByCodigo(Integer codigo){
        return listDTOParse(repository.findAllByCodigo(codigo));
    }

    public List<LogDTO> findAllByTipo(String tipo){
        return listDTOParse(repository.findAllByTipo(tipo));
    }

    public List<LogDTO> listDTOParse(List<LogEntity> logs){
        if(logs == null) return null;
        List<LogDTO> dtos = new ArrayList<>();
        for( LogEntity log : logs ){
            dtos.add(new LogDTO(log));
        }
        return dtos;
    }
}
