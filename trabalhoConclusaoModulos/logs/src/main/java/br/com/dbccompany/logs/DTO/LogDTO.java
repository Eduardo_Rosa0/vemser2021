package br.com.dbccompany.logs.DTO;

import br.com.dbccompany.logs.Entity.LogEntity;

public class LogDTO {
    private String descricao;

    private String tipo;

    private Integer codigo;

    public LogEntity converter(){
        return new LogEntity(this.descricao,this.tipo,this.codigo);
    }

    public LogDTO(LogEntity log) {
        this.descricao = log.getDescricao();
        this.tipo = log.getTipo();
        this.codigo = log.getCodigo();
    }

    public LogDTO(){}

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
}
