package br.com.dbccompany.logs.Repository;

import br.com.dbccompany.logs.Entity.LogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface LogRepository extends MongoRepository<LogEntity, String> {

    List<LogEntity> findAllByCodigo( Integer codigo );
    List<LogEntity> findAllByTipo( String tipo );

}
