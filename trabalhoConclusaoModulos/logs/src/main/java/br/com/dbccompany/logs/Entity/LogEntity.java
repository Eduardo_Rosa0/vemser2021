package br.com.dbccompany.logs.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document( collection = "LOG" )
public class LogEntity {

    @Id
    private String id;

    private String descricao;

    private String tipo;

    private Integer codigo;

    public LogEntity(String descricao, String tipo, Integer codigo) {
        this.id = id;
        this.descricao = descricao;
        this.tipo = tipo;
        this.codigo = codigo;
    }

    public LogEntity(){}
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
}
