package br.com.dbccompany.logs.Controller;

import br.com.dbccompany.logs.DTO.LogDTO;
import br.com.dbccompany.logs.Entity.LogEntity;
import br.com.dbccompany.logs.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "api/mangolona" )
public class LogController {

    @Autowired
    private LogService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<LogDTO> findAll(){
        return service.findAll();
    }

    @PostMapping( value = "/save")
    @ResponseBody
    public LogDTO save( @RequestBody LogEntity log ) {
        return service.save(log);
    }

    @GetMapping(value = "buscar/por/tipo/{tipo}")
    @ResponseBody
    public List<LogDTO> findAllByTipo ( @PathVariable String tipo ){
        return service.findAllByTipo(tipo);
    }

    @GetMapping(value = "buscar/por/codigo/{codigo}")
    @ResponseBody
    public List<LogDTO> findAllByCodigo( @PathVariable Integer codigo ){
        return service.findAllByCodigo(codigo);
    }

}
